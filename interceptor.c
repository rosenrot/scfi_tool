/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Jan Bickel, 2016
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>        /* the code belonging to the system call with number __NR_xxx defined in /usr/include/asm/unistd.h usr/include/asm/unistd_64.h*/
#include <linux/sched.h>           /* Gives information about the current Task. The variable current could be used for that*/
#include <linux/spinlock.h>
#include <linux/spinlock_types.h>
#include <linux/errno.h>           /* Get the errnos see man 3 errno for the list /usr/lib/modules/4.5.4-1-ARCH/build/include/uapi/asm-generic/errno.h */
#include <linux/random.h>          /* get_random_bytes */
#include <linux/proc_fs.h>         /* To interact with procfs */
#include <linux/types.h>
#include <linux/socket.h>
/*
 * Write Protect Bit (CR0:16)
 */
#define CR0_WP 0x00010000
/*
 * KERN_EMERG   - system is unusable
 * KERN_ALERT   - action must be taken immediately
 * KERN_CRIT    - critical conditions
 * KERN_ERR     - error conditions
 * KERN_WARNING - warning conditions
 * KERN_NOTICE  - normal but significant condition
 * KERN_INFO    - informational
 * KERN_DEBUG   - debug-level messages
 */
#define LOGLEVEL   KERN_INFO
#define DEBUG      0
#define NOSYSCALLS 327 // Total number of SysCalls in x86_64
#define TRUE       1
#define FALSE      0

MODULE_AUTHOR("Jan Bickel");
MODULE_LICENSE("GPL");


typedef unsigned socklen_t;
struct dirent
{
        ino_t d_ino;
        off_t d_off;
        unsigned short d_reclen;
        unsigned char d_type;
        char d_name[256];
};


static const char* mod_name =  "Failure injector";
static void **sys_call_table = NULL;

/*
 * target_pid  == pid to inject the error.
 * inj_errno   == error to be injected
 * syscall_no  == target syscall
 * invoc_count == count invocations of the syscalls
 */
static int target_pid  = 0;
static int inj_errno   = 0;
static int syscall_no  = 0;
static int invoc_count = 0;
static int finished    = 0;
module_param(target_pid  , int , 0644);
module_param(inj_errno   , int , 0644);
module_param(syscall_no  , int , 0644);
module_param(invoc_count , int , 0644);
module_param(finished    , int , 0644);

static atomic_t current_invoc[NOSYSCALLS];
static atomic_t current_finished_invoc[NOSYSCALLS];

/*
 * return true if there are invocations to be executed
 * otherwise return false
 */
int check_current_invoc(void) {
    for (int i = 0; i < NOSYSCALLS; i++) {
        if (atomic_read(&current_invoc[i]) != atomic_read(&current_finished_invoc[i])) {
            return true;
        }
    }
    return false;
}
int inc_invoc(int syscall_no) {
    atomic_inc(&current_invoc[syscall_no]);
    return atomic_read(&current_invoc[syscall_no]);
}
void dec_invoc(int syscall_no) {
    atomic_inc(&current_finished_invoc[syscall_no]);
    return;
}
void print_invoc_not_null(void) {
    for (int i = 0; i < NOSYSCALLS; i++) {
        if (atomic_read(&current_invoc[i])-atomic_read(&current_finished_invoc[i]) != 0) {
            printk( KERN_INFO "%s: %d - %d\n", mod_name, i, atomic_read(&current_invoc[i]) - atomic_read(&current_finished_invoc[i]));
        }
    }
}

int check_invoc(int current_invoc){
    return (current_invoc >= invoc_count && !finished) ? 1 : 0;
}

asmlinkage long (*original_mprotect)(void *addr, size_t len, int prot);

asmlinkage long new_mprotect(void *addr, size_t len, int prot) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_mprotect);

    if (ts->pid == target_pid && syscall_no == __NR_mprotect) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_mprotect(addr, len, prot);
        }
    }
    else {
        ret = original_mprotect(addr, len, prot);
    }

    dec_invoc(__NR_mprotect);
    return ret;
}

asmlinkage long (*original_read)(int fd, void *buf , size_t count);

asmlinkage long new_read(int fd, void *buf , size_t count) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_read);

    if (ts->pid == target_pid && syscall_no == __NR_read) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_read(fd, buf, count);
        }
    }
    else {
        ret = original_read(fd, buf, count);
    }

    dec_invoc(__NR_read);
    return ret;
}

asmlinkage long (*original_brk)(int inc);

asmlinkage long new_brk(int inc) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_brk);

    if (ts->pid == target_pid && syscall_no == __NR_brk) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_brk(inc);
        }
    }
    else {
        ret = original_brk(inc);
    }

    dec_invoc(__NR_brk);
    return ret;
}

asmlinkage long (*original_lseek)(int fd, off_t offset, int whence);

asmlinkage long new_lseek(int fd, off_t offset, int whence) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_lseek);

    if (ts->pid == target_pid && syscall_no == __NR_lseek) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_lseek(fd, offset, whence);
        }
    }
    else {
        ret = original_lseek(fd, offset, whence);
    }

    dec_invoc(__NR_lseek);
    return ret;
}

asmlinkage long (*original_munmap)(void *addr, size_t length);

asmlinkage long new_munmap(void *addr, size_t length) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_munmap);

    if (ts->pid == target_pid && syscall_no == __NR_munmap) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_munmap(addr, length);
        }
    }
    else {
        ret = original_munmap(addr, length);
    }

    dec_invoc(__NR_munmap);
    return ret;
}

asmlinkage long (*original_rt_sigprocmask)(int how, void *set , sigset_t *oldset , unsigned long nsig);

asmlinkage long new_rt_sigprocmask(int how, void *set , sigset_t *oldset , unsigned long nsig) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_rt_sigprocmask);

    if (ts->pid == target_pid && syscall_no == __NR_rt_sigprocmask) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_rt_sigprocmask(how, set, oldset, nsig);
        }
    }
    else {
        ret = original_rt_sigprocmask(how, set, oldset, nsig);
    }

    dec_invoc(__NR_rt_sigprocmask);
    return ret;
}

asmlinkage long (*original_access)(const char *pathname , int mode);

asmlinkage long new_access(const char *pathname , int mode) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_access);

    if (ts->pid == target_pid && syscall_no == __NR_access) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_access(pathname, mode);
        }
    }
    else {
        ret = original_access(pathname, mode);
    }

    dec_invoc(__NR_access);
    return ret;
}

asmlinkage long (*original_execve)(const char *filename, char *const argv[], char *const envp[]);

asmlinkage long new_execve(const char *filename, char *const argv[], char *const envp[]) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_execve);

    if (ts->pid == target_pid && syscall_no == __NR_execve) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_execve(filename, argv, envp);
        }
    }
    else {
        ret = original_execve(filename, argv, envp);
    }

    dec_invoc(__NR_execve);
    return ret;
}

asmlinkage long (*original_getrlimit)(int resource, struct rlimit *rlim );

asmlinkage long new_getrlimit(int resource, struct rlimit *rlim ) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_getrlimit);

    if (ts->pid == target_pid && syscall_no == __NR_getrlimit) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_getrlimit(resource, rlim);
        }
    }
    else {
        ret = original_getrlimit(resource, rlim);
    }

    dec_invoc(__NR_getrlimit);
    return ret;
}

asmlinkage long (*original_set_tid_address)(int *tidptr );

asmlinkage long new_set_tid_address(int *tidptr ) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_set_tid_address);

    if (ts->pid == target_pid && syscall_no == __NR_set_tid_address) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_set_tid_address(tidptr);
        }
    }
    else {
        ret = original_set_tid_address(tidptr);
    }

    dec_invoc(__NR_set_tid_address);
    return ret;
}

asmlinkage long (*original_gettimeofday)(struct timespec *tv , struct timezone *tz );

asmlinkage long new_gettimeofday(struct timespec *tv , struct timezone *tz ) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_gettimeofday);

    if (ts->pid == target_pid && syscall_no == __NR_gettimeofday) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_gettimeofday(tv, tz);
        }
    }
    else {
        ret = original_gettimeofday(tv, tz);
    }

    dec_invoc(__NR_gettimeofday);
    return ret;
}

asmlinkage long (*original_stat)(const char *pathname , struct stat *buf );

asmlinkage long new_stat(const char *pathname , struct stat *buf ) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_stat);

    if (ts->pid == target_pid && syscall_no == __NR_stat) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_stat(pathname, buf);
        }
    }
    else {
        ret = original_stat(pathname, buf);
    }

    dec_invoc(__NR_stat);
    return ret;
}

asmlinkage long (*original_fstat)(int fd, struct stat *buf );

asmlinkage long new_fstat(int fd, struct stat *buf ) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_fstat);

    if (ts->pid == target_pid && syscall_no == __NR_fstat) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_fstat(fd, buf);
        }
    }
    else {
        ret = original_fstat(fd, buf);
    }

    dec_invoc(__NR_fstat);
    return ret;
}

asmlinkage long (*original_close)(int fd);

asmlinkage long new_close(int fd) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_close);

    if (ts->pid == target_pid && syscall_no == __NR_close) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_close(fd);
        }
    }
    else {
        ret = original_close(fd);
    }

    dec_invoc(__NR_close);
    return ret;
}

asmlinkage long (*original_open)(const char *pathname , int flags, mode_t mode);

asmlinkage long new_open(const char *pathname , int flags, mode_t mode) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_open);

    if (ts->pid == target_pid && syscall_no == __NR_open) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_open(pathname, flags, mode);
        }
    }
    else {
        ret = original_open(pathname, flags, mode);
    }

    dec_invoc(__NR_open);
    return ret;
}

asmlinkage long (*original_accept)(int sockfd, struct sockaddr *addr , socklen_t *addrlen , long dummy1, long dummy2, long dummy3);

asmlinkage long new_accept(int sockfd, struct sockaddr *addr , socklen_t *addrlen , long dummy1, long dummy2, long dummy3) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_accept);

    if (ts->pid == target_pid && syscall_no == __NR_accept) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_accept(sockfd, addr, addrlen, dummy1, dummy2, dummy3);
        }
    }
    else {
        ret = original_accept(sockfd, addr, addrlen, dummy1, dummy2, dummy3);
    }

    dec_invoc(__NR_accept);
    return ret;
}

asmlinkage long (*original_recvfrom)(int sockfd, void *buf , size_t len, int flags, struct sockaddr *src_addr , socklen_t *addrlen);

asmlinkage long new_recvfrom(int sockfd, void *buf , size_t len, int flags, struct sockaddr *src_addr , socklen_t *addrlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_recvfrom);

    if (ts->pid == target_pid && syscall_no == __NR_recvfrom) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_recvfrom(sockfd, buf, len, flags, src_addr, addrlen);
        }
    }
    else {
        ret = original_recvfrom(sockfd, buf, len, flags, src_addr, addrlen);
    }

    dec_invoc(__NR_recvfrom);
    return ret;
}

asmlinkage long (*original_getdents)(int fd, struct dirent *buf , size_t len);

asmlinkage long new_getdents(int fd, struct dirent *buf , size_t len) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_getdents);

    if (ts->pid == target_pid && syscall_no == __NR_getdents) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_getdents(fd, buf, len);
        }
    }
    else {
        ret = original_getdents(fd, buf, len);
    }

    dec_invoc(__NR_getdents);
    return ret;
}

asmlinkage long (*original_sendto)(int, const void *, size_t, int, const struct sockaddr *, socklen_t);

asmlinkage long new_sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_sendto);

    if (ts->pid == target_pid && syscall_no == __NR_sendto) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_sendto(sockfd, buf, len, flags, dest_addr, addrlen);
        }
    }
    else {
        ret = original_sendto(sockfd, buf, len, flags, dest_addr, addrlen);
    }

    dec_invoc(__NR_sendto);
    return ret;
}

asmlinkage long (*original_write)(unsigned int, const char __user *, size_t);

asmlinkage long new_write(unsigned int fd, const char __user *buf, size_t count) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_write);

    if (ts->pid == target_pid && syscall_no == __NR_write) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_write(fd, buf, count);
        }
    }
    else {
        ret = original_write(fd, buf, count);
    }

    dec_invoc(__NR_write);
    return ret;
}

asmlinkage long (*original_nanosleep)(const struct timespec *rqtp, struct timespec *rmtp);

asmlinkage long new_nanosleep(const struct timespec *rqtp, struct timespec *rmtp){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_write);


    if (ts->pid == target_pid && syscall_no == __NR_nanosleep) {
    printk(KERN_INFO "CURRENTPID %d target_pid %d", ts->pid, target_pid);
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_nanosleep(rqtp, rmtp);
        }
    }
    else {
        ret = original_nanosleep(rqtp, rmtp);
    }
    dec_invoc(__NR_write);
    return ret;
}

asmlinkage long (*original_select)(int n, fd_set __user *inp, fd_set __user *outp,
                                   fd_set __user *exp, struct timeval __user *tvp);

asmlinkage long new_select(int n, fd_set __user *inp, fd_set __user *outp,
                           fd_set __user *exp, struct timeval __user *tvp) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_select);

    ret = original_select(n, inp, outp, exp, tvp);
    if (ts->pid == target_pid && syscall_no == __NR_select) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_select(n, inp, outp, exp, tvp);
        }
    }
    else { // Execute a normal select
        ret = original_select(n, inp, outp, exp, tvp);
    }
    dec_invoc(__NR_select);
    return ret;
}
asmlinkage long (*original_mmap)(void *addr, size_t length,
                                 int prot,   int flags,
                                 int fd,      off_t offset);

asmlinkage long new_mmap(void *addr, size_t length,
                         int prot,   int flags,
                         int fd,     off_t offset) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_mmap);


    if (ts->pid == target_pid && syscall_no == __NR_mmap) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_mmap(addr, length, prot, flags, fd, offset);
        }
    }
    else { // Execute a normal mmap
        ret = original_mmap(addr, length, prot, flags, fd, offset);
    }

    dec_invoc(__NR_mmap);
    return ret;
}

asmlinkage long (*original_rt_sigaction)(int signum, const struct sigaction __user *,
                                         struct sigaction __user *, size_t size);

asmlinkage long new_rt_sigaction(int signum, const struct sigaction __user *act,
                                 struct sigaction __user *oldact, size_t size) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_rt_sigaction);


    if (ts->pid == target_pid && syscall_no == __NR_rt_sigaction) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_rt_sigaction(signum, act, oldact, size);
        }
    }
    else { // Execute a normal rt_sigaction
        ret = original_rt_sigaction(signum, act, oldact, size);
    }

    dec_invoc(__NR_rt_sigaction);
    return ret;
}

asmlinkage long (*original_ioctl)(unsigned int fd, unsigned int command, unsigned long arg);

asmlinkage long new_ioctl(unsigned int fd, unsigned int command, unsigned long arg) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_ioctl);

    if (ts->pid == target_pid && syscall_no == __NR_ioctl) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_ioctl(fd, command, arg);
        }
    }
    else { // Execute a normal ioctl
        ret = original_ioctl(fd, command, arg);
    }

    dec_invoc(__NR_ioctl);
    return ret;
}

asmlinkage long (*original_writev)(unsigned long fd, const struct iovec __user *vec,
                                   unsigned long vlen);

asmlinkage long new_writev(unsigned long fd, const struct iovec __user *vec,
                           unsigned long vlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_writev);


    if (ts->pid == target_pid && syscall_no == __NR_writev) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_writev(fd, vec, vlen);
        }
    }
    else { // Execute a normal writev
        ret = original_writev(fd, vec, vlen);
    }

    dec_invoc(__NR_writev);
    return ret;
}

asmlinkage long (*original_socket)(int domain, int type, int protocol);

asmlinkage long new_socket(int domain, int type, int protocol) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_socket);


    if (ts->pid == target_pid && syscall_no == __NR_socket) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_socket(domain, type, protocol);
        }
    }
    else { // Execute a normal socket
        ret = original_socket(domain, type, protocol);
    }

    dec_invoc(__NR_socket);
    return ret;
}

asmlinkage long (*original_connect)(int sockfd, const struct sockaddr *addr, int addrlen);

asmlinkage long new_connect(int sockfd, const struct sockaddr *addr, int addrlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_connect);


    if(ts->pid == target_pid){
        printk(KERN_INFO "%s: JUST DEBUG pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
    }
    if (ts->pid == target_pid && syscall_no == __NR_connect) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_connect(sockfd, addr, addrlen);
        }
    }
    else { // Execute a normal connect
        ret = original_connect(sockfd, addr, addrlen);
    }

    dec_invoc(__NR_connect);
    return ret;
}

asmlinkage long (*original_bind)(int sockfd, const struct sockaddr *addr, int addrlen);

asmlinkage long new_bind(int sockfd, const struct sockaddr *addr, int addrlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_bind);


    if (ts->pid == target_pid && syscall_no == __NR_bind) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_bind(sockfd, addr, addrlen);
        }
    }
    else { // Execute a normal bind
        ret = original_bind(sockfd, addr, addrlen);
    }

    dec_invoc(__NR_bind);
    return ret;
}

asmlinkage long (*original_listen)(int sockfd, int backlog);

asmlinkage long new_listen(int sockfd, int backlog) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_listen);


    if (ts->pid == target_pid && syscall_no == __NR_listen) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_listen(sockfd, backlog);
        }
    }
    else { // Execute a normal listen
        ret = original_listen(sockfd, backlog);
    }

    dec_invoc(__NR_listen);
    return ret;
}

asmlinkage long (*original_getsockname)(int sockfd, struct sockaddr *addr, int *addrlen);

asmlinkage long new_getsockname(int sockfd, struct sockaddr *addr, int *addrlen) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_getsockname);


    if (ts->pid == target_pid && syscall_no == __NR_getsockname) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_getsockname(sockfd, addr, addrlen);
        }
    }
    else { // Execute a normal getsockname
        ret = original_getsockname(sockfd, addr, addrlen);
    }

    dec_invoc(__NR_getsockname);
    return ret;
}

asmlinkage long (*original_setsockopt)(int sockfd, int level, int optname,
                                       char __user *optval, int *optlen);

asmlinkage long new_setsockopt(int sockfd, int level, int optname,
                               char __user *optval, int *optlen){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_setsockopt);


    if (ts->pid == target_pid && syscall_no == __NR_setsockopt) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_setsockopt(sockfd, level, optname, optval, optlen);
        }
    }
    else { // Execute a normal setsockopt
        ret = original_setsockopt(sockfd, level, optname, optval, optlen);
    }

    dec_invoc(__NR_setsockopt);
    return ret;
}

asmlinkage long (*original_clone)(unsigned long flags, unsigned long first_unknown, int *ptid,
                                  int *ctid, unsigned long second_unknown);

asmlinkage long new_clone(unsigned long flags, unsigned long first_unknown, int *ptid,
                                  int *ctid, unsigned long second_unknown){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_clone);


    if (ts->pid == target_pid && syscall_no == __NR_clone) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_clone(flags, first_unknown, ptid, ctid, second_unknown);
        }
    }
    else { // Execute a normal clone
        //ret = original_clone(flags, child_stack, ptid, ctid, regs);
        ret = original_clone(flags, first_unknown, ptid, ctid, second_unknown);
    }

    dec_invoc(__NR_clone);
    return ret;
}

asmlinkage long (*original_fcntl)(unsigned int fd, unsigned int cmd, unsigned long arg);

asmlinkage long new_fcntl(unsigned int fd, unsigned int cmd, unsigned long arg) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_fcntl);


    if (ts->pid == target_pid && syscall_no == __NR_fcntl) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_fcntl(fd, cmd, arg);
        }
    }
    else { // Execute a normal fcntl
        ret = original_fcntl(fd, cmd, arg);
    }

    dec_invoc(__NR_fcntl);
    return ret;
}
asmlinkage int (*original_sigaltstack)(const stack_t *ss , stack_t *oss );

asmlinkage int new_sigaltstack(const stack_t *ss , stack_t *oss ){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_sigaltstack);


    if (ts->pid == target_pid && syscall_no == __NR_sigaltstack) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_sigaltstack(ss, oss);
        }
    }
    else { // Execute a normal sigaltstack
        ret = original_sigaltstack(ss, oss);
    }

    dec_invoc(__NR_sigaltstack);
    return ret;
}

asmlinkage long (*original_arch_prctl)(int code, unsigned long __user *addr);

asmlinkage long new_arch_prctl(int code, unsigned long __user *addr) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_arch_prctl);


    if (ts->pid == target_pid && syscall_no == __NR_arch_prctl) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_arch_prctl(code, addr);
        }
    }
    else { // Execute a normal arch_prctl
        ret = original_arch_prctl(code, addr);
    }

    dec_invoc(__NR_arch_prctl);
    return ret;
}

asmlinkage int (*original_futex)(int *uaddr , int op, int val,
                                 const struct timespec *timeout , int *uaddr2,
                                 int val3);

asmlinkage int new_futex(int *uaddr , int op, int val,
                                 const struct timespec *timeout , int *uaddr2,
                                 int val3){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_futex);


    if (ts->pid == target_pid && syscall_no == __NR_futex) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_futex(uaddr, op, val, timeout, uaddr2, val3);
        }
    }
    else { // Execute a normal futex
        ret = original_futex(uaddr, op, val, timeout, uaddr2, val3);
    }

    dec_invoc(__NR_futex);
    return ret;
}

asmlinkage long (*original_set_robust_list)(struct robust_list_head *head, size_t len);

asmlinkage long new_set_robust_list(struct robust_list_head *head, size_t len) {
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_set_robust_list);


    if (ts->pid == target_pid && syscall_no == __NR_set_robust_list) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_set_robust_list(head, len);
        }
    }
    else { // Execute a normal set_robust_list
        ret = original_set_robust_list(head, len);
    }

    dec_invoc(__NR_set_robust_list);
    return ret;
}


asmlinkage int (*original_sched_getaffinity)(pid_t pid, size_t cpusetsize, void *mask);

asmlinkage int new_sched_getaffinity(pid_t pid, size_t cpusetsize, void *mask){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_sched_getaffinity);


    if (ts->pid == target_pid && syscall_no == __NR_sched_getaffinity) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_sched_getaffinity(pid, cpusetsize, mask);
        }
    }
    else { // Execute a normal sched_getaffinity
        ret = original_sched_getaffinity(pid, cpusetsize, mask);
    }

    dec_invoc(__NR_sched_getaffinity);
    return ret;
}

asmlinkage int (*original_clock_gettime)(clockid_t clk_id, struct timespec *tp );

asmlinkage int new_clock_gettime(clockid_t clk_id, struct timespec *tp ){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_clock_gettime);


    if (ts->pid == target_pid && syscall_no == __NR_clock_gettime) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_clock_gettime(clk_id, tp);
        }
    }
    else { // Execute a normal clock_gettime
        ret = original_clock_gettime(clk_id, tp);
    }

    dec_invoc(__NR_clock_gettime);
    return ret;
}

asmlinkage int (*original_epoll_wait)(int epfd, struct epoll_event *events , int maxevents, int timeout);

asmlinkage int new_epoll_wait(int epfd, struct epoll_event *events , int maxevents, int timeout){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_epoll_wait);


    if (ts->pid == target_pid && syscall_no == __NR_epoll_wait) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_epoll_wait(epfd, events, maxevents, timeout);
        }
    }
    else { // Execute a normal epoll_wait
        ret = original_epoll_wait(epfd, events, maxevents, timeout);
    }

    dec_invoc(__NR_epoll_wait);
    return ret;
}

asmlinkage int (*original_epoll_ctl)(int epfd, int op, int fd, struct epoll_event *event );

asmlinkage int new_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event ){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_epoll_ctl);


    if (ts->pid == target_pid && syscall_no == __NR_epoll_ctl) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_epoll_ctl(epfd, op, fd, event);
        }
    }
    else { // Execute a normal epoll_ctl
        ret = original_epoll_ctl(epfd, op, fd, event);
    }

    dec_invoc(__NR_epoll_ctl);
    return ret;
}

asmlinkage int (*original_openat)(int dirfd, const char *pathname , int flags, mode_t mode);

asmlinkage int new_openat(int dirfd, const char *pathname , int flags, mode_t mode){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_openat);


    if (ts->pid == target_pid && syscall_no == __NR_openat) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_openat(dirfd, pathname, flags, mode);
        }
    }
    else { // Execute a normal openat
        ret = original_openat(dirfd, pathname, flags, mode);
    }

    dec_invoc(__NR_openat);
    return ret;
}

asmlinkage int (*original_epoll_create1)(int flags);

asmlinkage int new_epoll_create1(int flags){
    long ret = 0;
    struct task_struct* ts = current;
    int current_count = inc_invoc(__NR_epoll_create1);


    if (ts->pid == target_pid && syscall_no == __NR_epoll_create1) { 
        if (check_invoc(current_count)) {
            finished = 1;
            printk(KERN_INFO "%s: pid = %d inject %d to syscall %d\n", mod_name, ts->pid, inj_errno, syscall_no); 
            ret = -inj_errno;
        }
        else {
            ret = original_epoll_create1(flags);
        }
    }
    else { // Execute a normal epoll_create1
        ret = original_epoll_create1(flags);
    }

    dec_invoc(__NR_epoll_create1);
    return ret;
}

static void change_calls_init(void) {
    // disable interrupts
    spinlock_t m_lock = __SPIN_LOCK_UNLOCKED();
    unsigned long flags;
    unsigned long cr0;

    /* 
     * TODO
     * CHECK IF FLAG IS CURRENTLY SET OR NOT
     */
    /*
     * TODO
     * Check if the spin lock is used correctly
     */
    // aquire lock
    spin_lock_irqsave(&m_lock, flags);

    // read WP flag from cr0
    cr0 = read_cr0();

    // flipping the 16bit lets the CPU write to ro pages
    write_cr0(cr0 & ~CR0_WP);

    /* 
     * Save old syscall pointer
     */
    original_write             = (void*)sys_call_table[__NR_write];
    original_nanosleep         = (void*)sys_call_table[__NR_nanosleep];
    //original_select          = (void*)sys_call_table[__NR_select];
    original_mmap              = (void*)sys_call_table[__NR_mmap];
    original_rt_sigaction      = (void*)sys_call_table[__NR_rt_sigaction];
    original_ioctl             = (void*)sys_call_table[__NR_ioctl];
    original_writev            = (void*)sys_call_table[__NR_writev];
    original_socket            = (void*)sys_call_table[__NR_socket];
    original_connect           = (void*)sys_call_table[__NR_connect];
    original_bind              = (void*)sys_call_table[__NR_bind];
    original_listen            = (void*)sys_call_table[__NR_listen];
    original_getsockname       = (void*)sys_call_table[__NR_getsockname];
    original_setsockopt        = (void*)sys_call_table[__NR_setsockopt];
    original_fcntl             = (void*)sys_call_table[__NR_fcntl];
    original_arch_prctl        = (void*)sys_call_table[__NR_arch_prctl];
    original_set_robust_list   = (void*)sys_call_table[__NR_set_robust_list];
    //original_clone             = (void*)sys_call_table[__NR_clone];
    original_sigaltstack       = (void*)sys_call_table[__NR_sigaltstack];
    //original_futex             = (void*)sys_call_table[__NR_futex];
    original_sched_getaffinity = (void*)sys_call_table[__NR_sched_getaffinity];
    original_clock_gettime     = (void*)sys_call_table[__NR_clock_gettime];
    //original_epoll_wait        = (void*)sys_call_table[__NR_epoll_wait];
    original_epoll_ctl         = (void*)sys_call_table[__NR_epoll_ctl];
    original_epoll_create1     = (void*)sys_call_table[__NR_epoll_create1];
    original_openat            = (void*)sys_call_table[__NR_openat];

    /*
     * Change write
     */
    sys_call_table[__NR_write]             = new_write;
    sys_call_table[__NR_nanosleep]         = new_nanosleep;
    //sys_call_table[__NR_select]          = new_select;
    sys_call_table[__NR_mmap]              = new_mmap;
    sys_call_table[__NR_rt_sigaction]      = new_rt_sigaction;
    sys_call_table[__NR_ioctl]             = new_ioctl;
    sys_call_table[__NR_writev]            = new_writev;
    sys_call_table[__NR_socket]            = new_socket;
    sys_call_table[__NR_connect]           = new_connect;
    sys_call_table[__NR_bind]              = new_bind;
    sys_call_table[__NR_listen]            = new_listen;
    sys_call_table[__NR_getsockname]       = new_getsockname;
    sys_call_table[__NR_setsockopt]        = new_setsockopt;
    sys_call_table[__NR_fcntl]             = new_fcntl;
    sys_call_table[__NR_arch_prctl]        = new_arch_prctl;
    sys_call_table[__NR_set_robust_list]   = new_set_robust_list;
    //sys_call_table[__NR_clone]             = new_clone;
    sys_call_table[__NR_sigaltstack]       = new_sigaltstack;
    //sys_call_table[__NR_futex]             = new_futex;
    sys_call_table[__NR_sched_getaffinity] = new_sched_getaffinity;
    sys_call_table[__NR_clock_gettime]     = new_clock_gettime;
    //sys_call_table[__NR_epoll_wait]        = new_epoll_wait;
    sys_call_table[__NR_epoll_ctl]         = new_epoll_ctl;
    sys_call_table[__NR_epoll_create1]     = new_epoll_create1;
    sys_call_table[__NR_openat]            = new_openat;

    #if DEBUG
    printk(LOGLEVEL "%s: Write system call old address:           %p \n" , mod_name , original_write);
    printk(LOGLEVEL "%s: Write system call new address:           %p \n" , mod_name , new_write);
    printk(LOGLEVEL "%s: Nanosleep system call old address:       %p \n" , mod_name , original_nanosleep);
    printk(LOGLEVEL "%s: Nanosleep system call new address:       %p \n" , mod_name , new_nanosleep);
    //printk(LOGLEVEL "%s: select system call old address:          %p \n" , mod_name , original_select);
    //printk(LOGLEVEL "%s: select system call new address:          %p \n" , mod_name , new_select);
    printk(LOGLEVEL "%s: mmap system call old address:            %p \n" , mod_name , original_mmap);
    printk(LOGLEVEL "%s: mmap system call new address:            %p \n" , mod_name , new_mmap);
    printk(LOGLEVEL "%s: rt_sigaction system call old address:    %p \n" , mod_name , original_rt_sigaction);
    printk(LOGLEVEL "%s: rt_sigaction system call new address:    %p \n" , mod_name , new_rt_sigaction);
    printk(LOGLEVEL "%s: ioctl system call old address:           %p \n" , mod_name , original_ioctl);
    printk(LOGLEVEL "%s: ioctl system call new address:           %p \n" , mod_name , new_ioctl);
    printk(LOGLEVEL "%s: writev system call old address:          %p \n" , mod_name , original_writev);
    printk(LOGLEVEL "%s: writev system call new address:          %p \n" , mod_name , new_writev);
    printk(LOGLEVEL "%s: socket system call old address:          %p \n" , mod_name , original_socket);
    printk(LOGLEVEL "%s: socket system call new address:          %p \n" , mod_name , new_socket);
    printk(LOGLEVEL "%s: connect system call old address:         %p \n" , mod_name , original_connect);
    printk(LOGLEVEL "%s: connect system call new address:         %p \n" , mod_name , new_connect);
    printk(LOGLEVEL "%s: bind system call old address:            %p \n" , mod_name , original_bind);
    printk(LOGLEVEL "%s: bind system call new address:            %p \n" , mod_name , new_bind);
    printk(LOGLEVEL "%s: listen system call old address:          %p \n" , mod_name , original_listen);
    printk(LOGLEVEL "%s: listen system call new address:          %p \n" , mod_name , new_listen);
    printk(LOGLEVEL "%s: getsockname system call old address:     %p \n" , mod_name , original_getsockname);
    printk(LOGLEVEL "%s: getsockname system call new address:     %p \n" , mod_name , new_getsockname);
    printk(LOGLEVEL "%s: setsockopt system call old address:      %p \n" , mod_name , original_setsockopt);
    printk(LOGLEVEL "%s: setsockopt system call new address:      %p \n" , mod_name , new_setsockopt);
    printk(LOGLEVEL "%s: fcntl system call old address:           %p \n" , mod_name , original_fcntl);
    printk(LOGLEVEL "%s: fcntl system call new address:           %p \n" , mod_name , new_fcntl);
    printk(LOGLEVEL "%s: arch_prctl system call old address:      %p \n" , mod_name , original_arch_prctl);
    printk(LOGLEVEL "%s: arch_prctl system call new address:      %p \n" , mod_name , new_arch_prctl);
    printk(LOGLEVEL "%s: set_robust_list system call old address: %p \n" , mod_name , original_set_robust_list);
    printk(LOGLEVEL "%s: set_robust_list system call new address: %p \n" , mod_name , new_set_robust_list);
    #endif


    //Changing control bit back
    write_cr0(cr0);

    // release lock
    spin_unlock_irqrestore(&m_lock, flags);
}

static void change_calls_exit(void) {
    // disable interrupts
    spinlock_t m_lock = __SPIN_LOCK_UNLOCKED();
    unsigned long flags;
    unsigned long cr0;

    // Aquire lock
    spin_lock_irqsave(&m_lock, flags);

    // Read WP flag from cr0
    cr0 = read_cr0();

    // Flipping the 16bit lets the CPU write to ro pages
    write_cr0(cr0 & ~CR0_WP);

    // Change sys calls to original sys calls
    sys_call_table[__NR_write]             = original_write;
    sys_call_table[__NR_nanosleep]         = original_nanosleep;
    //sys_call_table[__NR_select]          = original_select;
    sys_call_table[__NR_mmap]              = original_mmap;
    sys_call_table[__NR_rt_sigaction]      = original_rt_sigaction;
    sys_call_table[__NR_ioctl]             = original_ioctl;
    sys_call_table[__NR_writev]            = original_writev;
    sys_call_table[__NR_socket]            = original_socket;
    sys_call_table[__NR_connect]           = original_connect;
    sys_call_table[__NR_bind]              = original_bind;
    sys_call_table[__NR_listen]            = original_listen;
    sys_call_table[__NR_getsockname]       = original_getsockname;
    sys_call_table[__NR_setsockopt]        = original_setsockopt;
    sys_call_table[__NR_fcntl]             = original_fcntl;
    sys_call_table[__NR_arch_prctl]        = original_arch_prctl;
    sys_call_table[__NR_set_robust_list]   = original_set_robust_list;
    //sys_call_table[__NR_clone]             = original_clone;
    sys_call_table[__NR_sigaltstack]       = original_sigaltstack;
    //sys_call_table[__NR_futex]             = original_futex;
    sys_call_table[__NR_sched_getaffinity] = original_sched_getaffinity;
    sys_call_table[__NR_clock_gettime]     = original_clock_gettime;
    //sys_call_table[__NR_epoll_wait]        = original_epoll_wait;
    sys_call_table[__NR_epoll_ctl]         = original_epoll_ctl;
    sys_call_table[__NR_epoll_create1]     = original_epoll_create1;
    sys_call_table[__NR_openat]            = original_openat;

    #if DEBUG
    printk(LOGLEVEL "%s: Sys call adresses on exit:", mod_name);
    printk(LOGLEVEL "%s: write system call current address:           %p \n", mod_name, sys_call_table[__NR_write]);
    printk(LOGLEVEL "%s: nanosleep system call current address:       %p \n", mod_name, sys_call_table[__NR_nanosleep]);
    //printk(LOGLEVEL "%s: select system call current address:          %p \n", mod_name, sys_call_table[__NR_select]);
    printk(LOGLEVEL "%s: mmap system call current address:            %p \n", mod_name, sys_call_table[__NR_mmap]);
    printk(LOGLEVEL "%s: rt_sigaction system call current address:    %p \n", mod_name, sys_call_table[__NR_rt_sigaction]);
    printk(LOGLEVEL "%s: ioctl system call current address:           %p \n", mod_name, sys_call_table[__NR_ioctl]);
    printk(LOGLEVEL "%s: writev system call current address:          %p \n", mod_name, sys_call_table[__NR_writev]);
    printk(LOGLEVEL "%s: socket system call current address:          %p \n", mod_name, sys_call_table[__NR_socket]);
    printk(LOGLEVEL "%s: connect system call current address:         %p \n", mod_name, sys_call_table[__NR_connect]);
    printk(LOGLEVEL "%s: bind system call current address:            %p \n", mod_name, sys_call_table[__NR_bind]);
    printk(LOGLEVEL "%s: listen system call current address:          %p \n", mod_name, sys_call_table[__NR_listen]);
    printk(LOGLEVEL "%s: getsockname system call current address:     %p \n", mod_name, sys_call_table[__NR_getsockname]);
    printk(LOGLEVEL "%s: setsockopt system call current address:      %p \n", mod_name, sys_call_table[__NR_setsockopt]);
    printk(LOGLEVEL "%s: fcntl system call current address:           %p \n", mod_name, sys_call_table[__NR_fcntl]);
    printk(LOGLEVEL "%s: arch_prctl system call current address:      %p \n", mod_name, sys_call_table[__NR_arch_prctl]);
    printk(LOGLEVEL "%s: set_robust_list system call current address: %p \n", mod_name, sys_call_table[__NR_set_robust_list]);
    #endif
    //Changing control bit back
    write_cr0(cr0);

    // release lock
    spin_unlock_irqrestore(&m_lock, flags);
}

/* https://bbs.archlinux.org/viewtopic.php?id=139406 */
static void *aquire_sys_call_table(void) {
    unsigned long int offset = PAGE_OFFSET;
    void **sct;

    while (offset < ULLONG_MAX) {
        sct = (void **)offset;

        if (sct[__NR_close] == (unsigned long *) sys_close) {
          return (void *) sct;
        }

        offset += sizeof(void *);
    }
    
    return NULL;
}

static int __init my_module_init(void) {
    printk(LOGLEVEL "%s: Init module!\n", mod_name);
    for (int i = 0; i < NOSYSCALLS; i++) {
        current_invoc[i] = (atomic_t) { (0) };
    }
    sys_call_table = aquire_sys_call_table();
    change_calls_init();
    if (sys_call_table == NULL) return 1;
    if (target_pid) {
        printk(LOGLEVEL "%s: target_pid  = %d", mod_name, target_pid);
        printk(LOGLEVEL "%s: syscall_no  = %d", mod_name, syscall_no);
        printk(LOGLEVEL "%s: inj_errno   = %d", mod_name, inj_errno);
        printk(LOGLEVEL "%s: invoc_count = %d", mod_name, invoc_count);

    }
    printk(LOGLEVEL "%s: Module initiated correctly", mod_name);
    return 0;
}

static void __exit my_module_exit(void) {
    printk(LOGLEVEL "%s: Exit module!\n", mod_name);
    /*
     * restore old syscall table
     */
    change_calls_exit();

    /*
     * Print pending syscalls and their invocation count
     * Spin till there are no old syscalls left
     */
    print_invoc_not_null();
    while (check_current_invoc()) { }
    printk(KERN_INFO "%s: Module exited cleanly", mod_name);
   return;
}

module_init(my_module_init);
module_exit(my_module_exit);
