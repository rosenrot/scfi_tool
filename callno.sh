#!/bin/bash

cat /usr/include/asm/unistd_64.h | grep $1 | sed 's/#define //' | sed 's/ / = /'
