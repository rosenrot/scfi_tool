#!/bin/bash

cat /usr/lib/modules/$(uname -r)/build/include/uapi/asm-generic/errno.h | grep $1 | sed 's/#define//'
cat /usr/lib/modules/$(uname -r)/build/include/uapi/asm-generic/errno-base.h | grep $1 | sed 's/#define//'
