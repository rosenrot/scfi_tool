obj-m += interceptor.o
# https://www.kernel.org/doc/Documentation/kbuild/modules.txt

ccflags-y := -g -ggdb -std=gnu99

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
