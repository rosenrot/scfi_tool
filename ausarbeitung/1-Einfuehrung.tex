\mychapter{Einführung}
Heute sind wir mehr und mehr auf Computerprogramme angewiesen. Ihre
Allgegenwärtigkeit zieht sich durch unser komplettes Leben, sei es im
Privaten oder auf der Arbeit. Sie sind teilweise sichtbar und offen zu
erkennen oder können kaum als solche wahrgenommen werden. In immer mehr
Bereichen sind wir davon abhängig, dass die genutzten Programme ihren Dienst
ordnungsgemäß erfüllen. Die fehlerfreie Entwicklung von solchen Programmen wird
jedoch durch immer komplexere Abhängigkeitsverhältnisse und schwer abschätzbare
Nebeneffekte stetig anspruchsvoller. 

Fehlerinjektion kann unter anderem dazu genutzt werden, die Robustheit von
Anwendungen zu erhöhen. Dies geschieht, indem für diese Anwendungen durch
Fehlerinjektion Stress simuliert wird. Mit dem so entstandenen Stress kann
besser geprüft werden, ob die Fehlerbehandlung wie gewünscht arbeitet. Die
Informationen aus solchen Tests können dann zur Verbesserung der bestehenden
Programme genutzt werden oder schon im Entwicklungsprozess Fehler und nicht
bedachte Grenzfälle aufzeigen. Mit diesem Vorgehen kann eine Vielzahl von
Szenarien getestet werden, die mit anderen Methoden kaum abdeckbar sind. 

\section{Motivation}
Da sensible Daten aktuell auf verteilten Systemen gespeichert und verarbeitet
werden ergeben sich Anforderungen an die entsprechenden Programme.  Hier muss
die Verfügbarkeit, Integrität und Vertraulichkeit der Daten gewahrt bleiben. In
erster Linie muss bei Programmen, die mit diesen Daten umgehen, versucht werden
die Datenschutzziele umzusetzen. Doch selbst wenn die Programme fehlerfrei
ablaufen und die entsprechenden Ziele einhalten, gibt es Kanäle, die das
vertrauenswürdige Programm nicht absichern kann. Das Betriebssystem zum Beispiel
kann ungehindert auf alle Daten zugreifen, Regionen im Speicher auslesen oder
Daten manipulieren.  Um dies zu verhindern, entwickelte Intel für die
Prozessoren des Typs Skylake die Software Guard Extensions (SGX), welche den
Zugriff auf bestimmte Speicherregionen des Hauptspeichers einschränkt und dies
mittels hardwarebasierter Verschlüsselung weiter absichert. Somit ist es nicht
mehr möglich, dass externe Programme oder das Betriebssystem auf diese Bereiche
zugreifen oder unbemerkt verändern. Da Programme, welche Software Guard
Extensions nutzen, jedoch weiterhin auf Speicher außerhalb des abgesicherten
Bereichs zugreifen können, stellt sich nun die Frage, ob trotz solcher oder
ähnlicher Sicherheitsmechanismen ein potentiell schadhaftes Betriebssystem
Programme dazu bringen kann, ihre Daten preiszugeben.

Diese Annahme führte zu der Überlegung, wie Fehlerinjektion auf
Betriebssystemebene aussehen könnte, die gegebenenfalls zu Fehlverhalten im
Programmablauf führen kann. Als Ansatzpunkt für ein neues
Fehlerinjektionswerkzeug wurde deshalb die Schnittstelle zwischen dem
privilegierten Betriebssystem-Kern und dem unprivilegierten Bereich der
Anwenderprogramme gewählt. Hier wird die Kommunikation zwischen
Betriebssystem-Kern und Programm durch Systemaufrufe geregelt, welche deshalb als
potentielles Risiko eingestuft werden müssen, da diese von einem schadhaften
Betriebssystem ausgenutzt werden können. Der in dieser Arbeit entwickelte
Fehlerinjektor soll deshalb ein fehlerhaftes beziehungsweise schadhaftes
Betriebssystem simulieren und so eventuelle Schwachstellen im Programmablauf
aufdecken.

\section{Zielsetzung}
Diese Arbeit soll sowohl den Entwurf und die praktische Umsetzung eines neuen 
Fehler\-in\-jek\-tions\-werk\-zeugs auf Systemaufrufebene behandeln wie auch den
praktischen Nutzen anhand von Beispielen verdeutlichen. Der Entwurf orientiert
sich an bereits existierenden Werkzeugen, die eine ähnliche Funktion bieten.  Es
soll gezeigt werden, wo sich dieses Werkzeug einordnen lässt, und wo die
konzeptionellen Unterschiede liegen. 

Es wird hierbei darauf geachtet, dass das Werkzeug generisch ist, das heißt
unabhängig von der verwendeten Programmiersprache funktioniert, und eine kurze
Laufzeit hat. Das genutzte Fehlermodell umfasst einmalig injizierte Fehler in
die Schnittstelle der Systemaufrufe. Diese Einfachheit führt zu einem
überblickbaren Parameterraum, was bei kleineren Testprogrammen sogar dazu führt,
dass alle möglichen Fehler tatsächlich auch getestet werden können. Diese
Vereinfachung des Fehlermodells ermöglicht es, das neue Konzept im Rahmen dieser
Arbeit umzusetzen. 

Der praktische Nutzen soll an verschiedenen Beispielen real existierender
Programme gezeigt und diskutiert werden. Hier soll das umgesetzte Werkzeug
verdeutlichen, wie die Funktionsweise ist und aufzeigen, wie viel manueller
Aufwand für die Vor- und Nachbereitung einer Fehlerinjektionskampagne benötigt
wird.

\section{Aufbau der Arbeit}
Nach dieser Einführung soll im Kapitel~\ref{section:foundation} die Grundlage
der Arbeit geklärt werden. Neben Begriffserklärungen wird dort vor allem die
Einordnung dieses Werkzeugs aufgezeigt. Die Einordnung wird anhand einer
geschichtlichen Entwicklung der Fehlerinjektion behandelt, welche die
verschiedenen Teilbereiche und die jeweiligen Einsatzzwecke hervorhebt. Es
werden ebenso Begriffe geklärt, die das Lesen und Verstehen der Arbeit
vereinfachen. 

Anschließend werden in Kapitel~\ref{section:refs} ähnliche Arbeiten aufgeführt
und die Unterschiede sowie Gemeinsamkeiten zu dem hier vorgestellten Werkzeug
diskutiert. Es werden dort sowohl Bausteine von Fehlerinjektionswerkzeugen als
auch spezielle Beispiele behandelt. Sinn und Zweck ist es einen Überblick über
den Stand der Technik in diesem Gebiet zu verschaffen und deutlich zu machen,
welche Funktionen anderer Werkzeuge übernommen wurden oder übernommen werden
können.

Der Entwurf wird daraufhin im Kapitel~\ref{section:design} skizziert, wobei hier
vor allem auf das Fehlermodell und das Systemmodell eingegangen wird. Es soll
dargestellt werden, warum entsprechende Entscheidungen gefällt wurden. Diese
Annahmen werden im weiteren Verlauf der Arbeit immer wieder aufgegriffen und
zeigen so, weshalb entsprechende Entscheidungen getroffen wurden.

Im Kapitel~\ref{section:implementation}, der Implementierung, wird die Umsetzung
der entsprechenden Annahmen des Entwurfs und der tatsächliche Aufbau des
Fehlerinjektionswerkzeugs gezeigt. Einzelne Programmkomponenten sollen erklärt
werden, um das umgesetzte Werkzeug verständlich darzustellen. Die
Implementierung orientiert sich hierbei weitestgehend an existierenden Konzepten
und verfolgt einen sehr einfachen Ansatz.

Im weiteren Verlauf soll das Werkzeug einfache Programme testen, um so den
Funktionsumfang und dessen Wirksamkeit zu verdeutlichen. Dies wird in
Kapitel~\ref{section:test} behandelt. Es soll anhand von verschiedenen
Messwerten wie Zeitaufwand, Abdeckung des Parameterraums oder Anzahl der
erkannten Fehler gezeigt werden, welchen tatsächlichen Mehrwert das neu
entwickelte Programm mit sich bringt. 

Die Diskussion soll zuletzt in Kapitel~\ref{section:discussion} anhand von
Versuchsergebnissen und den getroffenen Entwurfsentscheidungen bearbeitet
werden. Es soll kritisch hinterfragt werden, welche Einschätzungen überdacht
werden sollten und wo sich in der Praxis Probleme gezeigt haben, die zu Beginn
der Arbeit noch nicht absehbar waren.  Auch sollen hier
Erweiterungsmöglichkeiten aufgeführt werden, welche die Nutzung des
Fehlerinjektionswerkzeugs in der Zukunft verbessern können. 
