\mychapter{Verwandte Arbeiten}
\label{section:refs}
In diesem Kapitel soll ein Überblick über den derzeitigen Stand  der Technik
und einzelne historische Meilensteinen der Fehlerinjektion verschafft werden.
Hierzu werden sowohl Methoden, die bei einer Fehlerinjektion zum Einsatz kommen,
als auch Fehlerinjektionswerkzeuge im Speziellen aufgezeigt. Außerdem soll das
in dieser Arbeit diskutierte Fehlerinjektionswerkzeug von den dargestellten
Methoden und Werkzeugen abgegrenzt werden.

\section{Instrumentierung}
Instrumentierung wird im Feld der Softwareentwicklung
der Prozess genannt, bei dem der Quellcode oder die Binärdatei mit
Zusatzinformation angereichert wird, um daraus Schlüsse zu ziehen, wie das
Programm funktioniert. Diese Methode kann angewendet werden, um Fehlverhalten
nachzuvollziehen oder zu simulieren.

Das Verfahren kann so als Grundbaustein für auf Code-Mutation basierende
Software-Fehlerinjektion genutzt werden.  Wie oben erwähnt gibt es hierbei zwei
Ansätze, die je nach Bedarf Verwendung finden. Zuerst sei an dieser Stelle
die manuelle Abänderung des Quellcodes genannt.  Hierbei werden im Quelltext
zusätzliche Funktionen eingeführt, die bestimmte Aufgaben, wie konkrete
Fehlerinjektion, übernehmen. Es benötigt hierfür Zugriff auf den Quelltext des
Programms und eine anschließende Kompilierung oder teilweise Kompilierung.

Im Gegensatz dazu sei die dynamische Instrumentierung binärer Dateien genannt.
Bei diesem Verfahren werden bereits kompilierte Programme instrumentiert, indem
diese direkt abgeändert werden. Als Beispiel für ein solchen Verfahren wird das
Framework Pin in Kapitel~\ref{section:intelpin} erläutert. Diese Methode kann
als Grundlage für eine Fehlerinjektion dienen. 

Bei einer Fehlerinjektion können mit der dynamischen Instrumentierung während
der Laufzeit durch die direkt abgeänderten Programme Fehler generiert und in das
Ziel eingegeben werden. Hierzu wird meist ein externer Prozess genutzt, der auf
Benutzerebene mit dem zu testenden Programm interagiert.

\subsection{Intel Pin}
\label{section:intelpin}
Intel\textsuperscript{\textregistered} Pin ist ein Werkzeug, mit dem dynamische
Softwareinstrumentierung von Benutzerprogrammen durchgeführt werden kann.
Bemerkenswert ist hierbei, dass es keinen Zugriff auf den Quellcode benötigt.
Dies ermöglicht leicht jede Art von Programmen zu instrumentieren. Es kann so
für spezielle Arten der Software-Fehlerinjektion genutzt werden. Das Werkzeug
nutzt als Basis das nicht instrumentierte Programm und ändert dieses durch
dynamische Kompilierung während der Laufzeit ab. Dabei wird laut
\cite{luk2005pin} auch eine kurze Ausführungszeit erreicht. Die instrumentierten
Programme werden dabei als Kindprozesse gestartet, sodass Pin einfach die
genutzten Ressourcen überwachen kann. Das ist zum Beispiel von Vorteil, wenn
der Speicherverbrauch oder andere Ressourcen überwacht werden sollen.

Pin könnte so eine Basis für ein Software-Fehlerinjektionswerkzeug bieten und
liefert in der derzeitigen Form auch eine Vielzahl von Funktionen, welche die
Entwicklung eines solchen vereinfachen. 

In Verbindung mit Intel\textsuperscript{\textregistered} SGX funktioniert Pin
jedoch nur, wenn der Simulationsmodus genutzt wird. Da die Motivation dieser
Arbeit das Testen von Programmen unter realen Bedingungen war, wurde auf die
Nutzung von Pin als grundlegenden Baustein verzichtet.

\subsection{Dyninst}
Eine Alternative zu Pin bietet Dyninst \cite{buck2000api}. Auch mit diesem
Framework kann Instrumentierung der Binärdateien während der Laufzeit
durchgeführt werden. Es ist plattformunabhängig und ermöglicht einen Gebrauch
auf Benutzerebene. Im Gegensatz zu Pin werden bei diesem Framework die zu
instrumentierenden Programme nicht als Kindprozess gestartet, sondern als
separater Prozess. An diesen wird der Hauptprozess, oder Mutator, gekoppelt. Der
Mutator kann nun mittels ptrace oder dem procfs Dateisystem auf das Programm
zugreifen und dieses verändern. 

Mit Dyninst können so Funktionen ausgetauscht, Variablen verändert, ganze
Module getauscht oder andere Aktionen durchgeführt werden.

Da Dyninst ptrace nutzt, kann es nicht ohne Einschränkung eingesetzt werden.
Teilweise ist diese Funktion systemweit nicht verfügbar oder bei dem Zielprozess
eingeschränkt. Mit SGX lässt sich ptrace auch nur im Simulationsmodus der
Enclaven nutzen, in der normalen Betriebsumgebung jedoch nicht.

\section{Fehlerinjektion}
In diesem Teil der Arbeit werden einige konkrete Fehlerinjektionswerkzeuge
behandelt. Da dies ein sehr weites Feld ist und, wie in
\ref{section:FI_development} beschrieben, sowohl Software- als auch
Hardware-Fehlerinjektionswerkzeuge existieren, soll die Auswahl der Werkzeuge
etwas eingeschränkt werden. Aus diesem Grund werden anschließend ausschließlich
Software-Fehlerinjektionswerkzeuge diskutiert.

\subsection{FIG}
Dieses Werkzeug, welches in \cite{broadwell2002fig} beschrieben wurde, nutzt die
Schnittstelle zwischen Applikation und Bibliothek. Der Unterschied zu
vorhergehenden Programmen wie \cite{kropp1998automated} ist, dass die Fehler
nicht in die Schnittstelle zwischen Nutzer und Anwenderprogramm eingegeben
werden, sondern auf einer niedrigeren Ebene. Zur Eingabe der Fehler werden
veränderte Teile der Zielbibliotheken genutzt, die mittels dynamischer
Verknüpfung vor der eigentlichen Bibliothek geladen werden und so die
ursprünglichen Funktionen überdecken. Mit Hilfe der auf diese Weise
ausgetauschten Funktionen kann die Fehlerinjektion leicht durchgeführt werden.

Die Gemeinsamkeit mit dem in der Arbeit beschriebenen Fehlerinjektionswerkzeug
ist, dass keine Hardwarefehler emuliert werden sollen, sondern eine Fehlfunktion
des Betriebssystems. Die Ebene, auf der beide Werkzeuge arbeiten, ist daher eine
ähnliche. In beiden Werkzeugen werden fehlerhafte Systemaufrufe simuliert. Der
Unterschied ist, dass die Anwendung von FIG sich auf einige Systemaufrufe wie
\textit{malloc}, \textit{open}, \textit{close}, \textit{select}, \textit{read}
und \textit{write} beschränkt, wobei hingegen das in dieser Arbeit beschriebene
Werkzeug die Wahl der Systemaufrufe dem Benutzer überlässt.  Die Einschränkungen
können so besser auf das Zielobjekt angepasst werden.  Außerdem ist hier der
genaue Ort der Schnittstelle wichtig. Bei FIG befindet sich dieser zwischen
Bibliothek und Programm, was den Einsatzzweck auf dynamisch verknüpfte Programme
beschränkt. Bei dem hier gezeigten Werkzeug befindet sich dagegen die
Schnittstelle zwischen Betriebssystem und Programm, was den Einsatz auch für
statisch verknüpfte Programme erlaubt.

\subsection{LFI}
\cite{marinescu2009lfi} zeigt, dass LFI wie FIG, die Schnittstelle zwischen
Bibliothek und Programm angreift. Eine deutliche Weiterentwicklung ist hierbei,
dass Techniken wie statische Analyse des Programms, genutzt werden, um bestimmte
Pfade im Programmablauf zu selektieren, in denen Fehler eingegeben werden und um
Seitenkanäle auszumachen. Dies ermöglicht eine gezieltere Fehlereingabe in
bestimmte Programmpfade und steigert weiterhin die Abdeckung des getesteten
Codes.

Dieses Vorgehen soll die manuelle Arbeit zum Einstellen des
Software-Fehlerinjektionswerkzeugs minimieren und so eine breiter gefächerte
Verwendung ermöglichen. Auch bei LFI ist es nicht möglich, in statisch
verknüpfte Programme Fehler zu injizieren. 

\subsection{PAIN}
In \cite{winter2015no} wird beschrieben, dass man durch parallele Eingabe der
Fehler die Testzeit verkürzen und gleichzeitig testen kann, wie ein Testsystem,
in diesem Fall ein Linux-Kern, mit einer Vielzahl von Fehlern umgeht. Es
unterscheidet sich insofern, als das in der Arbeit gewählte Fehlermodell
wesentlich einfacher ist und aufeinanderfolgende Fehler nicht beinhaltet. Das
elaboriertere Verfahren, welches in \cite{winter2015no} eingesetzt wurde, ist
zwar in der Lage andere Fehler zu detektieren, jedoch ist der parallele
Testaufbau nicht trivial einzurichten und befindet sich so noch in einem schwer
einzusetzenden Stadium. Um falsch-positive Fehler zu vermeiden, muss die
parallele Eingabe der Fehler aufwendig eingestellt werden und ist wohl in vielen
Anwendungsbereichen nicht sinnvoll. 

\subsection{AFEX}
Bei AFEX soll vor allem die in \cite{banabic2012fast} beschriebene komplexere
Methodik der Fehlerinjektion betrachtet werden. Die Fehlerinjektion wird bei
AFEX nicht zufällig oder nach festen Wahrscheinlichkeitsverteilungen
vorgenommen, sondern nutzt ein adaptives Verfahren, welches auf verschiedenen
Metriken beruht. Die Wahrscheinlichkeiten der Testparameter werden anhand von
vorherigen Testergebnissen angepasst und erhöhen so die Wahrscheinlichkeit, ein
Fehlverhalten zu verursachen. Zu Beginn eines Testlaufs wird mit einer
zufälligen Verteilung gestartet. Falls durch eine spezielle Kombination der
Testparameter kein Fehlverhalten im Testsystem erzeugt wird, wird die
Wahrscheinlichkeit dieser Kombination gesenkt. Im Falle eines Fehlverhaltens
wird die Wahrscheinlichkeit für die Parameterkombination erhöht. Auf dieser
Basis werden neue Kombinationen ausgewählt, wobei versucht wird die Chance, bei
der nächsten Fehlerinjektion eine Reaktion zu erzeugen, zu maximieren.

Dieses Modell ermöglicht es, unter einer Vielzahl von Kombinationsmöglichkeiten
die auszuwählen, die für die Fehlerinjektion am interessantesten erscheinen. Der
aktuelle Stand des hier gezeigten Werkzeugs kann darauf verzichten, da aufgrund
des einfachen Fehlermodells durch den gesamten Parameterraum iteriert werden
kann.

\subsection{EDFI}
Im Gegensatz zu den bisher genannten Vorgehensweisen nutzt EDFI, welches in
\cite{giuffrida2013edfi} behandelt wurde, statische und dynamische
Instrumentierung, um sowohl die Abdeckung zu erhöhen als auch die
Reproduzierbarkeit der Versuche zu verbessern. Zur Fehlerinjektion nutzt das
Werkzeug dynamische Instrumentierung und kann damit statisch verknüpfte
Bibliotheken angreifen. Dynamisch verknüpfte müssen deshalb einzeln
instrumentiert werden. Da EDFI die Programme während der Laufzeit verändert, ist
dieses Verfahren außerhalb des SGX-Simulationsmodus nicht einsetzbar. Weiterhin
ist nur mit Einschränkungen ersichtlich wie schwierig die Nutzung des realen
Werkzeugs ist. 

\subsection{Systemaufruf-Fuzzer}
Bei Trinity (\cite{Jones2015trinity}) und Syzkaller (\cite{Ding2016Syzkaller})
handelt es sich um Software-Fehlerinjektionswerkzeuge, welche zum Testen von
Betriebssystemen, im Speziellen Linux, genutzt werden. Bei dieser Art von
Fuzz-Testing-Werkzeugen werden fehlerhafte Parameter an Systemaufrufe übergeben
um zu analysieren, wie sich das Testsystem verhält.  Es wird also der
Rückgabewert der Aufrufe und das Verhalten des Testsystems ausgewertet. Bei dem
in der Arbeit beschriebenen Werkzeug wird ein fehlerhaftes Betriebssystem
angegeben und dessen Verhalten simuliert. \cite{Jones2015trinity} zielt auf
Systemaufrufe ab und gibt in diese auf eine intelligente Weise Fehler ein, das
heißt Fehler, die trotzdem den Anforderungen des Systemaufrufs entsprechen.
Hierzu werden aufrufspezifische Vorlagen genutzt.

Bei \cite{Ding2016Syzkaller} wird, neben den schon bei Trinity verwendeten
systemaufrufspezifischen Vorlagen, Instrumentierung angewandt, mit deren Hilfe
eine geführte Fehlerinjektion möglich ist, die sich an interessanten Pfaden
orientiert. So wird die Wahrscheinlichkeit erhöht, ein Fehlverhalten zu
provozieren. 

Wie bei dem neu entwickelten Werkzeug werden die Fehler an der Schnittstelle
zum Betriebssystem, also den Systemaufrufen, übergeben. Es unterscheidet sich
jedoch insofern, als nicht das Betriebssystem auf ein Fehlverhalten getestet
werden soll, sondern ein Zielprogramm im Benutzer-Modus. Auch wird bei den hier
aufgezeigten Systemaufruf-Fuzzern angenommen, dass eine Vielzahl von
fehlerhaften Systemaufrufen zu einem Fehlverhalten führt. In dieser Arbeit
werden lediglich Einzelfehler und keine Fehlerketten behandelt.

\section{Zusammenfassung}
Wie in diesem Kapitel gezeigt, bietet der Bereich der Software-Fehlerinjektion
eine breite Basis mit vielen Möglichkeiten. Dabei gibt es eine Vielzahl von
möglichen Interaktionspunkten mit den zu testenden Programmen, wobei weder
dynamische Instrumentierung und Code-Mutation statischer Programme noch die
Nutzung von Eigenheiten dynamischer Verknüpfung vollumfänglich für das in dieser
Arbeit diskutierte Anwendungsgebiet nutzbar sind. Diese Einsicht ermöglicht es,
den Fokus auf die Schnittstellenfehlerinjektion im Bereich der Systemaufrufe zu
legen.

Weiterhin zeigen die genannten Programme einige Funktionen, wie statische
Analyse oder die Nutzung von Metriken, die in der weiteren Entwicklung des in
dieser Arbeit gezeigten Fehlerinjektionswerkzeugs interessant ist.
