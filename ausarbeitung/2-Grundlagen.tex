\mychapter{Grundlagen}
\label{section:foundation}
Dieses Kapitel beinhaltet Wissen, welches das Lesen der  Arbeit erleichtert.
Zuerst soll Fehlerinjektion generell diskutiert werden, indem einzelne
Teilbereiche dieser und die Entwicklung skizziert werden.  Anschließend werden
Methodiken der Fehlerinjektion vorgestellt. Zuletzt wird auf Linux-Kernel-Module
und Systemaufrufe eingegangen, da diese vom neu entwickelten
Fehlerinjektionswerkzeug genutzt werden.

\section{Fehlerinjektion}
Von heutiger Relevanz in der Forschung und Praxis ist vor allem die
Software-Fehlerinjektion. Da in der vorliegenden Arbeit auch nur dieser Bereich
behandelt wird, beschränkt sich die Definition der Fehlerinjektion vor allem
darauf.  \enquote{Software-Fehlerinjektion ist eine etablierte Technik um die
Robustheit eines Testsystems zu evaluieren, indem dieses in seiner
Betriebsumgebung mit Fehlern konfrontiert wird} \cite[sinngemäß
nach][]{winter2015no}. Die Techniken der Fehlerinjektion setzen also ein System
unter künstlichen Stress, um so auf eventuelles Fehlverhalten rückzuschließen.
Dadurch werden im Gegensatz zu Modultests, welche in der
Softwareentwicklung breite Anwendung finden, vermehrt auch unvorhergesehene
Pfade im Programm getestet.

Daraus ergeben sich zahlreiche Vor- und Nachteile. Als Nachteil sei vor allem
der große Testaufwand genannt, der mit Fehlerinjektion einhergeht. Dieser
beschränkt sich nicht nur auf das Einrichten einer Testumgebung, sondern vor
allem auch auf die Laufzeit der Tests selbst. Da diese oft zufällig in das
Testsystem eingegeben werden und, je nach Einsatzzweck, sehr viele
Fehlerkombinationen injiziert werden können, ist hier oft keine feste Laufzeit
absehbar. Weiterhin sei erwähnt, dass das Einrichten der Testumgebung und das
Auswählen des richtigen Fehlerinjektionswerkzeugs im Gegensatz zu Modultests um
ein Vielfaches größer ist.

Vorteilhaft ist aber, dass die hohe Abdeckung und Nutzung zufällig
eingegebenen Fehlern oft in der Lage ist Fehlverhalten zu provozieren, die bei
der Erstellung von Modultests kaum vorhersehbar waren. Dies ist möglich, da auch
Grenzfälle getestet werden können die bei Modultests leicht vergessen werden.

Das Anwenden von Fehlerinjektion verbessert so die Erkennung von Schwachstellen
und kann in Teilbereichen, in denen Robustheit eine große Rolle spielt, diese
signifikant erhöhen.

Nach der obigen Einführung in die Fehlerinjektion soll kurz die geschichtliche
Entwicklung dieses Teilbereichs der Informatik aufgezeigt werden, um so den
derzeitigen Stand der Technik besser verständlich zu machen.

\subsection{Entwicklung der Fehlerinjektion}
\label{section:FI_development}
Die Fehlerinjektion wurde bereits im Jahr 1967 in \cite{hardie1967design}
umgesetzt. Die Arbeit behandelt logische Schaltkreise, die Fehler in Form
getauschter oder hängengebliebener Bits in ein Computersystem eingeben konnte.
Diese Fehlerinjektion simulierte so fehlerbehaftete Hardware-Komponenten und
untersuchte, wie sich dieses Fehlverhalten auf die Programmlogik auswirkt,
welche mit dieser Hardware umgehen muss. 

Eine Weiterentwicklung dessen wurde in \cite{avizienis1978fault} bewerkstelligt.
In der Forschung zu dieser Arbeit wurden nicht nur einfach vorherzusehende
Fehler in Schaltkreisen, sondern auch physische Fehler auf dem Testsystem
umgesetzt. Der ausschlaggebende Grund hierfür war, dass Einflüsse von
Strahlungen oder elektromagnetische Interferenzen nicht so leicht modellierbar
waren wie fehlerhafte Schaltkreise.  Es werden also Fehlermodelle betrachtet,
die mit herkömmlichen Methoden nicht modellierbar sind. Diese haben nicht nur
Auswirkungen auf die Entwicklung von Software, welche mit solchen Fehlern
umgehen muss, sondern auch Einfluss auf die Hardwareentwicklung, bei der
teilweise die entsprechende Art von Fehlern vorher nicht bedacht wurde.

Diese beiden ursprünglichen Verfahren führten jedoch dazu, dass oftmals eine
Wiederholung der Tests schwierig bis unmöglich war. Die Einflüsse von Strahlung
oder Magnetfeldern sind in der Gänze schwer zu reproduzieren und hatten als Ziel
oft fehlertolerante Hardware. Um diese für die Softwareentwicklung relevanten
Probleme zu beseitigen, konzentrierte man sich vermehrt auf die Nachahmung von
Fehlersystemen, die in Form von Programmen umgesetzt wurden. Als geschichtlicher
Vorreiter sei hier die Arbeit von Arlat et al. \cite{arlat1990fault} zu nennen.
Das in der Arbeit beschriebene Vorgehen versuchte immer noch fehlerhafte
Computer-Komponenten zu ersetzen und befindet sich somit auf dem Übergang von
Hardware-Fehlerinjektion zur Software-Fehlerinjektion.

Eine Wende zeichnete sich mit \cite{segall1995fiat} ab. Der Fokus der
Fehlerinjektion richtete sich mehr und mehr auf das Testen der Robustheit von
Programmen. Im Zentrum der Forschung standen nun nicht mehr die genutzten
Computer-Komponenten, sondern bestimmte Testprogramme, die in einer Testumgebung
laufen. Dieser Schritt machte die Fehlerinjektion immer mehr für den Prozess der
Softwareentwicklung interessant. Hierbei wurde nun weniger fehlerhafte Hardware
nachgeahmt, sondern konkrete Fehler in der Logik der entwickelten Programmen
betrachtet. Es wurden also nicht mehr nur Fehler an der
Hardware-/Softwareschnittstelle betrachtet, sondern auch komplexere Fehlertypen,
die sich auf den geregelten Ablauf im Programm auswirken.

Der Paradigmenwechsel ermöglichte es die Fehlerinjektion auf verschiedene für
die Softwareentwicklung relevante Teilbereiche auszudehnen. Es wurden neue
Ansätze verwirklicht, welche sich in verschiedene Teilbereiche der
Fehlerinjektion gliedern lassen.

\subsection{Teilbereiche der Software-Fehlerinjektion}
Bei der Software-Fehlerinjektion gibt es unterschiedliche Angriffspunkte, die
genutzt werden können. Diese ermöglichen es unterschiedliche Arten von Fehlern
in das Testsystem einzugeben. Die verschiedenen Arten, Fehler in das System
zu injizieren, haben unterschiedliche Auswirkungen und können zur Analyse
unterschiedlicher Reaktionen genutzt werden. Im Folgenden wird auf verschiedene
Teilbereiche eingegangen.

Im einfachsten Fall werden Fehler direkt als invalide Parameter in das Programm
eingegeben. Es werden also bestehende Schnittstellen genutzt, in denen während
des Programmaublaufs Daten in das Testsystem gelangen. Diese Fehlerinjektion
befindet sich also auf Protokollebene.  Mit dieser Form der
Software-Fehlerinjektion lassen sich deshalb leicht Grenzfälle abdecken, indem
an den unterschiedlichsten Bereichen des möglichen Eingaberaums Fehler übergeben
werden und anschließend analysiert wird, wie sich das Testsystem verhält. Bei
der Anwendung mit randomisierten Eingabegeneratoren spricht man hier auch von
Fuzz Testing. Der Vorteil an dieser Art von Fehlerinjektion ist vor allem, dass
sie mit Programmen genutzt werden kann, deren Quelltext nicht zugänglich ist.
Außerdem können so Anwendungen im Produktiveinsatz einem Stresstest unterzogen
werden, indem fehlerhafte Eingaben durch den Nutzer oder falsch übermittelte
Daten simuliert werden. Hier ist der Vorteil, dass Fehler, die bei der
Entwicklung von Programmen nicht absehbar waren oder erst im Nachhinein
entstanden sind, weiterhin getestet werden können. Anwendung findet dieses
Verfahren aber nicht nur auf der Ebene von einfachen Eingabefeldern bei
Programmen für den Endnutzer, sondern auch als Parameter von Systemaufrufen wie
in den Beispielen von \cite{Jones2015trinity} und \cite{Ding2016Syzkaller}. 

Eine weitere Methode, die hauptsächlich im Bereich verteilter Systeme zum Tragen
kommt, ist das gewollte provozieren von Ausfällen. Hierbei werden einzelne
Komponenten des Testsystems zufällig abgeschaltet oder überlastet, um
selbstheilende Funktionen des Systems unter realen Bedingungen zu testen. Als
populäres Beispiel ist Chaos Monkey \cite{bennet2012monkey} anzuführen. Der
Fokus liegt dabei nicht auf einzelnen Programmkomponenten, sondern auf der
Fehlertoleranz eines kompletten Systems. Dieses wird bei einem Testlauf unter
Stress gesetzt und man testet auf vielfältige Art die Behandlung der
aufgetretenen Fehler. Es handelt sich also um eine grobe Form der
Fehlerinjektion, die versucht systematische Fehler auszumachen.

Weiterhin kann angeführt werden, dass es ein gängiges Verfahren ist, Fehler an
Schnittstellen der Komponenten direkt in das Testsystem einzugeben. Hier bieten
sich eine Vielzahl von Anknüpfungspunkten an. Meist wird auf der Ebene zwischen
dem eigentlichen Programm und seinen genutzten Bibliotheken gearbeitet. Um dies
zu bewerkstelligen, können Bibliotheken modifiziert werden oder
Einzelkomponenten die tatsächliche Bibliothek überdecken. Die Fehler werden bei
diesem Verfahren nicht mehr auf der Ebene spezifizierter
Kommunikationsprotokolle eingegeben, sondern direkt an gewählten Angriffspunkten
in das Testsystem injiziert. Dieses Verfahren kann genutzt werden, um während
der Entwicklung schwerwiegende Manipulationen im Programmablauf vorzunehmen oder
um schadhafte Komponenten zu simulieren.  Als Vertreter dieser Gruppe der
Software-Fehlerinjektion sind  \cite{winter2015no} und \cite{marinescu2009lfi}
zu nennen.

\subsection{Mechanismen der Software-Fehlerinjektion}
Wie im vorherigen Kapitel bereits angedeutet wurde, kann die
Software-Fehlerinjektion auf zwei grundlegende Mechanismen unterteilt werden:
Schnittstellenfehlerinkjektion auf der einen Seite und
Code-Mutation auf der anderen.

Bei der Schnittstellenfehlerinkjektion werden vorhandene Schnittstellen der
Programme genutzt, um Fehler auf einem bestehenden Weg an das Testsystem zu
übergeben. Diese können leicht getauscht oder manipuliert werden. Es ist dabei
nicht nötig das Testsystem zu verändern. Das Verfahren bietet so die Möglichkeit
Tests durchzuführen ohne die Logik des Testsystems zu kennen, um ein unbekanntes
Testsystem zu überprüfen (Black-Box-Tests). Das Ziel sind interne Fehlfunktionen
des Testsystems, die von außen provoziert werden sollen. Es können zum
Beispiel Fehler ausgelöst werden, die durch unsachgemäße Nutzung von Komponenten
entstehen.

Im Gegensatz dazu werden bei Code-Mutation Komponenten des Testsystem verändert,
um gewollt ein Fehlverhalten hervorzurufen. Es ist also meist Zugriff auf den
Quelltext erforderlich. Jedoch können Mutationen auch auf Binärdateien
angewendet werden.

Die beiden Mechanismen sollen in Grafik~\ref{fig:SSoftware-FehlerinjektionCMFI}
veranschaulicht werden.

\begin{figure}
\centering
\subfigure[Schnittstellenfehlerinkjektion]{
    \includegraphics[width=0.35\textwidth]{img/schnittstellen_fi.png}
}
\subfigure[Code-Mutation]{
    \includegraphics[width=0.35\textwidth]{img/codemut_fi.png}
}
\caption{Veranschaulichung für Schnittstehlen-Fehlerinjektion und Code-Mutation}
\label{fig:SSoftware-FehlerinjektionCMFI} 
\end{figure}

Der Vorteil von Code-Mutation zeigt sich vor allem, wenn spezifische
Fehlermodelle in zugänglichen Testsystemen implementiert werden sollen. Bei
diesem Verfahren behält man viel Kontrolle darüber wann, wo und wie Fehler
eingesetzt werden und kann feingranular Verteilungen von bekannten Fehlern
umsetzen.  Beispielsweise können mittels Code-Mutation einfach fehlerhafte
Schleifen im Programmablauf oder andere programmiertechnische Fehler umgesetzt
werden. Bei der Schnittstellenfehlerinkjektion hingegen kann ausgenutzt werden,
dass Komponenten einfach austauschbar sind. Das Testsystem benötigt keine
Anpassung. Dies ermöglicht es auch nach der Veröffentlichung der Programme
weiter zu testen, was vor allem bei Applikationen, die aus dem Internet
erreichbar sein sollen von Vorteil ist.

\section{Linux-Kernel-Modul}
Das hier besschriebene Fehlerinjektionswerkzeug nutzt als zentralen Baustein ein
ladbares Linux-Kernel-Modul zur Injektion der Fehler.  \enquote{Solche
Module können vom Linux-Kernel dynamisch während der Laufzeit geladen oder
entfernt werden. Dies ist dadurch möglich, dass dieses Modul aus nicht
verknüpftem Objektcode besteht und erst durch das Verknüpfen mit dem laufenden
Kern funktionstüchtig wird. Hierzu kann der Befehl insmod genutzt
werden. Zum Lösen der Verknüpfung, das heißt zum Entfernen des Moduls, kann der
Befehl rmmod genutzt werden}\cite[sinngemäß nach][]{corbet2005linux}.

Das Nutzen eines ladbaren Kernel-Moduls hat den Vorteil, dass Funktionsweisen
des Linux-Kerns getauscht werden können ohne den gesamten Kern neu zu
kompilieren, beziehungsweise das gesamte Betriebssystem neu zu laden. Dies
erleichtert die Entwicklung neuer Komponenten des Linux-Kerns. Da das Kern-Modul
nur zur Fehlerinjektion benötigt wird, ist die Ladbarkeit dieser Komponente
auch willkommen, da dieses Modul sonst nicht vom Betriebssystem benötigt wird.
Es kann also nach der Durchführung einer Fehlerinjektionskampagne leicht wieder
entfernt werden.

Im Vergleich zu normalen C-Bibliotheken unterscheiden sich ladbare Kernel-Module
in der Hinsicht, dass sie im Kernel-Modus ausgeführt werden und so Zugriff auf
interne Funktionalitäten des Systems haben. Es kann deshalb auf Speicherbereiche
zugegriffen werden, die von normalen Benutzerprogrammen nicht erreichbar sind.
Auch können ganze Systemkomponenten verändert werden. Diesen Vorteil macht sich
das Fehlerinjektionswerkzeug zu Nutze. Der Nachteil bei der Entwicklung eines
Kernel-Moduls besteht darin, dass keine Standardbibliotheken wie glibc verwendet
werden können. Dies verkompliziert die Entwicklung teilweise, da gewohnte
Datenstrukturen oder Algorithmen nicht genutzt werden können.

\section{Systemaufruf}
Da in der Domäne der Betriebssysteme der Zugriff auf Hardware-Ressourcen aus
Sicherheitsgründen eingeschränkt werden muss, werden Programme mit
unterschiedlichen Sicherheitsstufen ausgeführt. Diese Stufen werden auch
Ringe genannt und trennen im Fall von Linux den Kernel-Modus vom
Benutzer-Modus. Ein typisches Anwendungsprogramm wird im Nutzer-Modus ausgeführt
und muss benötigte Ressourcen vom Betriebssystem-Kern beantragen. Die
Interaktion mit dem Betriebssystem-Kern wird über Systemaufrufe getätigt. Sie
stellen die Schnittstelle zwischen Kern und Anwenderprogrammen dar und
ermöglichen die Interaktion der beiden Bereiche.  So kann ein Nutzerprogramm
beispielsweise auf Netzwerkgeräte zugreifen und mit diesen kommunizieren. In
Grafik~\ref{fig:Ring} wurde dieses Modell bildlich dargestellt. Der Kern hat
hier die meisten Privilegien, die über die Ringe der Gerätetreiber bis
zu den Benutzerprogrammen abnehmen.

\begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{img/ring.png}
    \caption{Ringe des Linux Kerns}
    \label{fig:Ring} 
\end{figure}

\newpage
Im Fall von Linux existieren circa 380 Systemaufrufe.  Diese lassen sich grob in
die fünf nachfolgenden Kategorien einteilen:
\begin{enumerate} \itemsep0em
    \item Prozesskontrolle
    \item Dateiverwaltung
    \item Geräteverwaltung
    \item Informationsverwaltung
    \item Kommunikation
\end{enumerate}

Die Interaktion zwischen Kern und Programm wird meist nicht direkt zwischen
diesen Komponenten durchgeführt, sondern durch Nutzung von
Programmierschnittstellen (API) vereinfacht, welche die entsprechenden
Systemaufrufe in Form von Bibliotheksfunktionen implementiert haben. So können
Funktionen wie \textit{open} oder \textit{close} leicht genutzt werden, ohne
dass der vollzogene Kontextwechsel von Nutzern oder Entwicklern bemerkt
wird. Kontextwechsel ist ein Vorgang, bei dem die sequenzielle Abarbeitung eines
Prozesses unterbrochen wird, um zu einem anderen Programm zu wechseln. Nach dem
Zurückkehren aus dem Wechsel wird der Stand des Prozesses vor dem Wechsel
wiederhergestellt und der Programmablauf fortgeführt. Dieser Kontextwechsel
ermöglicht es unprivilegierten Prozessen Zugriff auf System-Internas zu
verschaffen, ohne ihren unprivilegierten Status zu verändern. 

Die Nutzung der Systemaufrufe in dem Fehlerinjektionswerkzeug wird in
Kapitel~\ref{section:Sys_model} sowie in Kapitel~\ref{section:LKM_impl}
erläutert.
