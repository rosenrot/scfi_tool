\mychapter{Implementierung}
\label{section:implementation}
Es wird in diesem Kapitel die praktische Umsetzung des Werkzeugs behandelt.
Hier werden vor allem Implementierungsdetails, welche die Mechanismen des
Werkzeugs verdeutlichen, hervorgehoben. Die Gliederung des Kapitels orientiert
sich  dabei an der Struktur des Fehlerinjektionswerkzeugs. 

\section{Aufbau der Komponenten}
Wie in Kapitel~\ref{section:Sys_model} schon beschrieben, ist das
Software-Fehlerinjektionswerkzeug in drei Komponenten gegliedert. Diese
Komponenten werden in den folgenden Kapiteln genauer beschrieben, wobei deren
Funktion und Arbeitsweise ausgearbeitet werden soll.

Die Teilung in die einzelnen Komponenten hat den Vorteil, dass das Werkzeug
leichter erweiterbar ist. Die drei Module haben distinkte Funktionalitäten,
weshalb sich die logische Trennung in die Module anbietet.

Das Kontroll-Modul initialisiert die Komponenten und koordiniert die
Injektionen. Das Detektions-Modul behandelt Anfragen an das zu testende Ziel und
detektiert Fehler. Zuletzt ist das Kernel-Modul zu erwähnen, welches die
manipulierte Schnittstelle ist, an der die Fehler eingegeben werden. 

\subsection{Kontroll-Modul}
Mit diesem Modul wird die Fehlerinjektion gestartet. Es regelt sowohl die
Fehlereingabe als auch die Initialisierung, Steuerung und Termination der
restlichen Module der Fehlerinjektion und des Zielprogramms. Die Steuerung des
Kontroll-Moduls erfolgt über die eigene Konfigurationsdatei.

Beim Start des Kontroll-Moduls wird zuerst die Konfigurationsdatei gelesen. In
dieser Datei sind die für die Fehlerinjektion benötigten Daten angegeben. Dies
sind der Pfad zum zu testenden Programm, die Liste der Fehler, die injiziert
werden können, die auch die maximale Anzahl der Ausführungen der einzelnen
Systemaufrufe beinhaltet, die Rundenanzahl, die festlegt wie oft Fehler
injiziert werden sollen, eine Datei, in der alle Fehler protokolliert werden und
eine Datei, die eine Zuordnung von Systemaufrufen zu den möglichen Fehlern
beinhaltet. Nachdem die Konfigurationsdatei eingelesen wurde, kann der Testlauf
gestartet werden. Dieser umfasst folgende Schritte:

\begin{enumerate}
  \itemsep0em
  \item Gabeln des Prozesses für die auszuführende Datei
  \item Initialisieren des Detektions-Moduls
  \item Initialisieren des Kernel-Moduls
  \item Start der auszuführenden Datei
  \item Start der Injektionskampagne 
\end{enumerate}

Das Gabeln des Prozesses wird benötigt, um die Prozessnummer des Zielprozesses
zu erhalten, bevor die eigentliche Datei ausgeführt wird. Diese Prozessnummer
benötigt das Kernel-Modul, um die Fehlerinjektion vorzubereiten. Sie muss daher
vor dem Ausführen des Testprogramms vorhanden sein, da auf die Nutzung von
ptrace zum Anhalten von Prozessen verzichtet werden soll.

Für die konkrete Fehlerinjektion bietet das Kontroll-Modul zwei Möglichkeiten.
Die Injektionskampagne kann rundenbasiert ausgeführt werden. In diesem Fall
führt das Kontroll-Modul $n$ zufällig gewählte Fehlerinjektionen aus,
wobei $n$ der Rundenanzahl aus der Konfigurationsdatei entspricht. Pro
Runde wird ein zufälliger Systemaufruf aus der Liste der Systemaufrufe gewählt
und ermittelt, wann der Fehler eingegeben werden soll. Hierfür wird eine
ganzzahlige Zahl im Intervall von eins bis zur maximalen Zahl von Aufrufen des
entsprechenden Systemaufrufs ausgewählt. Die Kombination aus Systemaufruf und
dem Zeitpunkt der Injektion wird zwischengespeichert, damit nicht mehrmals
derselbe Fehler in das Testprogramm eingegebem wird. Im anderen Fall kann eine
gesamte Injektionskampagne durchgeführt werden. Falls diese Methode gewählt
wurde, werden alle möglichen Kombinationen aus Systemaufruf, eingegebenem Fehler
und Aufrufindex in das Testprogramm injiziert.

\subsection{Kernel-Modul}
\label{section:LKM_impl}
Das Kernel-Modul ist die zentrale Komponente der Fehlerinjektion. In ihm wird
der Fehler in das Zielprogramm eingegeben. Um dies zu realisieren werden die
Systemaufrufe mit Wrapper-Funktionen verdeckt, die den bestimmten Fehler in
das Zielprogramm eingeben.

In diesen Funktionen wird die Anzahl der Ausführungen des Systemaufrufs gezählt,
um den Fehler im richtigen Aufruf einzugeben. Zu diesem Zweck wird beim Eintritt
in die Funktion ein atomarer Zähler für den entsprechenden Systemaufruf
inkrementiert.

Beim Laden des Kernel-Moduls wird zuerst die Systemaufruftabelle gesucht, um in
diese die Wrapper-Funktionen direkt einzutragen. Der Prozess des Austauschens
der Funktionen in der Systemaufruftabelle wird durch eine Unterbrechungs-Sperre
abgesichert, damit während dieser Zeit kein anderer Prozess auf die Tabelle
zugreift. Sobald der Prozess abgesichert wurde, werden die Adressen zu den
echten Systemaufrufen gesichert und die Wrapper-Funktionen in die Tabelle
geschrieben. Nach Aufhebung der Sperre werden nicht mehr die ursprünglichen
Systemaufrufe abgehandelt, sondern die jeweiligen Wrapper-Funktionen.

Die Wrapper-Funktionen verhalten sich für alle Programme, die nicht die
Prozessnummer des Zielprogramms haben, wie der ursprüngliche Systemaufruf. Es
werden also nur die Funktionszeiger zu den echten Systemaufrufen ausgeführt. So
werden Programme, die nicht das Ziel der Fehlerinjektion sind, nicht vom
Kernel-Modul beeinträchtigt.

Um eine fehlerfreie Entfernung des Moduls zu gewährleisten werden alle Aufrufe
der Wrapper-Funktionen gezählt. Hier wird bei jedem Eintritt in die Funktion ein
Wert für den spezifischen Systemaufruf in einer Liste erhöht. Bei jeder
Beendigung wird ebenfalls ein Wert in einer anderen Liste erhöht.

Beim Entfernen des Moduls werden zuerst wieder die ursprünglichen Zeiger zu den
Systemaufrufen in die Systemaufruftabelle geschrieben. Anschließend werden die
beiden Listen der aufgerufenen und beendeten Wrapper-Funktionen verglichen.
Falls bei diesem Vergleich eine Differenz auftritt, muss das Entfernen des
Moduls verzögert werden, da noch nicht alle Systemaufrufe, die über die
Wrapper-Funktionen abgehandelt werden, zurückgetragen wurden. Sobald dies der
Fall ist kann das Modul sicher wieder entfernt werden.

Beim Start werden an das Kernel-Modul vier Parameter zur Fehlerinjektion
übergeben. Diese sind die \textit{Zielprozessnummer}, die
\textit{Systemaufrufnummer}, der zu \textit{injizierende Fehler} und der
\textit{Aufrufindex}, bei der der Fehler abgegeben werden soll. Nach dem Laden
erfasst das Modul für die jeweilige Zielprozessnummer die Anzahl der getätigten
Systemaufrufe. Anschließend prüft es bei jedem neuen Aufruf ob alle drei
Parameter, die den speziellen Systemaufruf klassifizieren, dem Zielaufruf
entsprechen, bei dem der Fehler abgegeben werden soll. Diese sind also die
Prozessnummer, der Systemaufruf und die Nummer des entsprechenden Aufrufs. Falls
dies nicht der Fall ist, wird der ursprüngliche Systemaufruf ausgeführt, im
anderen Fall wird der schon festgelegte Fehler zurückgegeben.

\subsection{Detektions-Modul}
\label{section:Det_impl}
Um Fehlverhalten zu detektieren, wurde ein Modul geschrieben, welches den
Zielprozess überwacht. Dieses Detektions-Modul sendet Anfragen an das Ziel und
überprüft im Anschluss die Ausgabe.

Beim Start des Moduls wird zuerst die Konfigurationsdatei eingelesen. Diese
beinhaltet den Programmpfad zur Binärdatei des Ziels, die zu sendenden
Arbeitsladung, ein Zeitintervall, in dem die Anfragen beantwortet werden müssen
und Einstellungen, die angeben an welchem Ort die Testdateien gesichert werden
sollen. Nach dem Einlesen der Konfigurationsparameter wird ein Testlauf mit der
angegeben Binärdatei und Anfrage gestartet. Die Ausgabe wird gesichert und
später zum Vergleich herangezogen.

Das Detektions-Modul erkennt vier Zustände. Diese sind \textit{Kein Fehlerhaftes
Verhalten}, \textit{Absturz}, \textit{Hängengeblieben} und \textit{Fehlerhafte
Ausgabe}. Nach dem Start der Fehlerinjektion werden diese Zustände geprüft.
Falls das Programm innerhalb der in der Konfiguration angegebenen Zeitspanne auf
die Anfrage antwortet und diese identisch mit der des Testlaufs ist, hat sich
kein fehlerhaftes Verhalten gezeigt. Falls das Ziel nicht mehr vorhanden ist und
keine Anfrage gesendet werden kann, wird der Testlauf als \textit{Absturz}
klassifiziert. Wenn das Ziel zwar vorhanden ist, aber die Anfrage in der gegeben
Zeitspanne nicht behandelt wurde, wird der Test als \textit{Hängengeblieben}
gewertet. Falls die Anfrage zwar im Zeitintervall behandelt wurde aber die
Antwort vom gesicherten Testlauf abweicht, wird eine \textit{Fehlerhafte
Ausgabe} registriert.

Falls ein Fehlverhalten auftritt, wird dies dem Kontroll-Modul mitgeteilt,
welches den entsprechenden Fehler protokolliert.
