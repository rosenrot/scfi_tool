# README #

This repository will be used for the software I am developing for my research
paper on a system call level fault injection tool.

### What is this repository for? ###

This fault injection tool should intercept system calls and occasionally inject
errors.

It is not jet sure if ptrace or a loadable kernel module (LKM) should be used
for doing so. 

### How do I get set up? ###

To compile this project the specific kernel headers are needed.

> make

"make" compiles the module.

After that move the interceptor.ko to the driver directory.

To perform an injection campaign use strace -c to obtain a list of used
systemcalls and their invocation counts.

After that build up a systamcall number - invocation count mapping where the
systemcall number and invocation cound are seperated by a "," and save it to a
invocation_count.csv file.

Also build up a csv which contains all systemcall numbers and the error numbers
which should be returned by the fault injection tool. Also save this file as
csv.

Now configure the FIController and FIDetector as done in the examples of the
Mongoose server and Go server. 

Then execute the FIController.py as root.

After the injection campaign remove the kernel-module by executing rmmod
interceptor.

The output could be found in the error_log.txt and run_statistic.csv.
