#!/bin/python3

import os
import time
import errno
import signal
import subprocess 
import socket
import difflib
import re
import csv
import configparser 
import shutil
import glob

class NoExecutableError(Exception):
    pass

class GoldenRunError(Exception):
    pass

class TimeoutError(Exception):
    pass

class Timeout(object):
    """Timeout class using ALARM signal."""

    def __init__(self, seconds):
        self.seconds = seconds

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, exception_type, exception_value, traceback):
        signal.alarm(0)
 
    def raise_timeout(self, *args):
        print("TIMEOUT!")
        raise TimeoutError("Timeout!")

class FIDetector(object):
    # TODO Change this to run executables without output file!
    def _perform_golden_run(self):
        FNULL = open(os.devnull, 'w')
        p_exe = subprocess.Popen(self.executable.split(' '), stdout=FNULL, stderr=FNULL, close_fds=True)
        # TODO better check if port is open
        time.sleep(1)
        if self.request:
            FNULL = open(os.devnull, 'w')
            p_request = subprocess.Popen(self.request.split(' '), stdout=FNULL, stderr=FNULL, close_fds=True)
        while p_request.poll() is None:
            time.sleep(0.2)
        p_exe.kill()

        while p_exe.poll() is None:
            time.sleep(0.2)

        if self.test_comp and self.test_output:
            os.rename(self.test_output, self.test_dir+"/"+self.test_comp)

    def __read_stats(self):
        if not os.path.isfile("run_statistic.csv"):
            self._results = [0, 0, 0, 0]
            with open("run_statistic.csv", 'a'):
                os.utime("run_statistic.csv", None)
        else:
            with open("run_statistic.csv", "r") as fd_stat:
                reader = csv.reader(fd_stat)
                row_c = 0
                for row in reader:
                    if row_c == 1:
                        for col in row:
                            self._results.append(int(col))
                    row_c = row_c + 1
                if row_c == 1:
                    self._results = [0, 0, 0, 0]
        return

    def __write_stats(self):
        with open("run_statistic.csv", "w") as fd_stat:
            writer = csv.writer(fd_stat, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            writer.writerow(["No Error", "Crash", "Hang", "Wrong Output"])
            writer.writerow(self._results)

    def __init__(self, target_pid):
        self.target_pid = target_pid
        self.saved_path = os.getcwd()
        config_array = []
        self._results = []
        # Get results of previouse test runs
        self.__read_stats()
        # Get config options
        with open('FIDetector_config.conf', 'r') as config_file:
            config = configparser.ConfigParser()
            config.readfp(config_file)
            try:
                self.executable = config.get('FIDetector', 'executable')
            except configparser.NoOptionError:
                raise NoExecutableError("No executable in FIDetector_config.conf")
            try:
                self.request = config.get('FIDetector', 'request')
            except configparser.NoOptionError:
                self.request = ''
            try:
                self.test_dir = config.get('FIDetector', 'test_dir')
            except configparser.NoOptionError:
                self.test_dir = 'test_dir'
            try:
                self.test_output = config.get('FIDetector', 'test_output')
            except configparser.NoOptionError:
                self.test_output = ''
            try:
                self.test_comp = config.get('FIDetector', 'test_comp')
            except configparser.NoOptionError:
                self.test_comp = ''
            try:
                self.timeout = int(config.get('FIDetector', 'timeout'))
            except configparser.NoOptionError:
                self.timeout = 10
        if not os.path.exists(self.test_dir):
            os.mkdir(self.test_dir, mode=0o744)
        if os.path.isdir(self.test_dir):
            pass
        else:
            raise LookupError
        self._perform_golden_run()

    def _compare_results(self):
        if self.test_comp and self.test_output:
            try:
                file_1 = open(self.test_dir+"/"+self.test_output, 'r')
            except IOError:
                return -2
            with file_1, open(self.test_dir+"/"+self.test_comp, 'r') as file_2:
                lines_1 = file_1.readlines()
                lines_2 = file_2.readlines()
                lines_1_striped = [re.sub(r'(\d{2}-\D{3}-\d{4} \d{2}:\d{2})|(\D{3}, \d{2} \D{3} \d{4} \d{2}:\d{2}:\d{2} GMT)', '', elem) for elem in lines_1]
                lines_2_striped = [re.sub(r'(\d{2}-\D{3}-\d{4} \d{2}:\d{2})|(\D{3}, \d{2} \D{3} \d{4} \d{2}:\d{2}:\d{2} GMT)', '', elem) for elem in lines_2]
                for line in difflib.unified_diff(lines_1_striped, lines_2_striped, fromfile="file1", tofile="file2", lineterm='', n=0):
                    for prefix in ('---', '+++', '@@'):
                        if line.startswith(prefix):
                            break
                    else:
                        return -1

    # TODO Don't just check if process is still there but also if it responds
    def __clean_test_dir(self):
        for element in glob.glob("index.html*"):
            os.unlink(element)
        if os.path.isfile(self.test_dir+"/"+self.test_output):
            os.unlink(self.test_dir+"/"+self.test_output)

    def check_once(self):
        # Check if target is present before sending the request
        try:
            with Timeout(self.timeout):
                os.kill(self.target_pid, 0)
        except OSError:
            print("CRASH: Target no more present before request.")
            self._results[1] = self._results[1] + 1
            self.__clean_test_dir()
            self.__write_stats()
            return -1
        except TimeoutError:
            print("HANG: Signal timed out before request.")
            self._results[2] = self._results[2] + 1
            self.__clean_test_dir()
            self.__write_stats()
            return -2

        # If a request has to be send do the following
        if self.request:
            try:
                with Timeout(self.timeout):
                    FNULL = open(os.devnull, 'w')
                    p_request = subprocess.Popen(self.request.split(' '), stdout=FNULL, stderr=FNULL, close_fds=True)
                    while p_request.poll() is None:
                        time.sleep(0.2)
                    if p_request.poll() != 0:
                        print("CRASH: Request could not be handled!")
                        self._results[1] = self._results[1] + 1
                        self.__clean_test_dir()
                        self.__write_stats()
                        return -1
                    else:
                        try:
                            file_1 = open(self.test_output, 'r')
                        except:
                            print("HANG: No Output present!")
                            self._results[2] = self._results[2] + 1
                            self.__clean_test_dir()
                            self.__write_stats()
                            return -2
                        file_1.close()
                        os.rename(self.test_output, self.test_dir+"/"+self.test_output)
            except TimeoutError:
                print("HANG: Request timed out.")
                self._results[2] = self._results[2] + 1
                self.__clean_test_dir()
                self.__write_stats()
                return -2
            try:
                with Timeout(self.timeout):
                    os.kill(self.target_pid, 0)
            except OSError:
                print("CRASH: Target no more present after request.")
                self._results[1] = self._results[1] + 1
                self.__clean_test_dir()
                self.__write_stats()
                return -1
            except TimeoutError:
                print("HANG: Signal timed out after request.")
                self._results[2] = self._results[2] + 1
                self.__clean_test_dir()
                self.__write_stats()
                return -2

            cmp_ret = self._compare_results()
            if  cmp_ret == -1:
                print("FAILUR: Output not equal!")
                self._results[3] = self._results[3] + 1
                self.__clean_test_dir()
                self.__write_stats()
                return -3
            elif cmp_ret == -2:
                print("HANG: No Output present!")
                self._results[2] = self._results[2] + 1
                self.__clean_test_dir()
                self.__write_stats()
                return -2

            self._results[0] = self._results[0] + 1
            print("No Error")

        self.__clean_test_dir()
        self.__write_stats()
        return 0
