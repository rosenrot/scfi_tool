#!/bin/python3

import FIDetector 
import subprocess
import signal
import os
import time
import sys
import getopt
import errno
import psutil
import csv
import configparser 
import random
from functools import wraps
# http://stackoverflow.com/questions/4648792/is-there-a-python-module-to-parse-linuxs-sysfs
from os.path import isdir, isfile, islink, join, realpath, normpath
from keyword import iskeyword

_norm = lambda name: name + ('_' if iskeyword(name) else '')

def _denorm(name):
    if name.endswith('_') and iskeyword(name[:-1]):
        return name[:-1]
    else:
        return name

def _norm_path(path):
    return normpath(realpath(path))

class SysFsObject(object):
    __slots__ = ['_path', '__dict__']

    @staticmethod
    def __id_args__(path='/sys'):
        return _norm_path(path)

    def __init__(self, path='/sys'):
        self._path = _norm_path(path)
        if not self._path.startswith('/sys'):
            raise RuntimeError("Using this on non-sysfs files is dangerous!")
        self.__dict__.update(dict.fromkeys(_norm(i) for i in os.listdir(self._path)))

    def __repr__(self):
        return "<SysFsObject %s>" % self._path

    def __setattr__(self, name, val):
        if name.startswith('_'):
            return object.__setattr__(self, name, val)
        name = _denorm(name)
        p = realpath(join(self._path, name))
        if isfile(p):
            with open(p, 'w') as param:
                param.write(str(val))
        else:
            raise RuntimeError

    def __getattribute__(self, name):
        if name.startswith('_'):
            return object.__getattribute__(self, name)
        name = _denorm(name)
        p = realpath(join(self._path, name))
        if isfile(p):
            data = open(p, 'r').read()[:-1]
            try:
                return int(data)
            except ValueError:
                return data
        elif isdir(p):
            return SysFsObject(p)

# The module needs to be removed after execution. Idea: another kernel_param which shows if the call was injected. This means bussy waiting?

class FIController(object):
    def __init__(self):
        self.used_param = {}
        self.__read_config()

    def _check_load_lkm(self):
        FNULL = open(os.devnull, 'w')
        p_lsmod = subprocess.Popen("lsmod", stdout=subprocess.PIPE)
        p_grep = subprocess.Popen(["grep", "interceptor"], stdin=p_lsmod.stdout, stdout=subprocess.PIPE)
        while p_grep.poll() is None:
            time.sleep(0.2)
        return p_grep.poll() # 1 = not loaded; 0 = loaded

    def inject_once(self, syscall_no, inj_errno, invoc_count):
        if not os.path.isfile(self.log_file):
            with open(self.log_file, 'a'):
                os.utime(self.log_file, None)
            
        self.syscall_no = syscall_no
        self.inj_errno = inj_errno
        self.invoc_count = invoc_count
        # Check if the module is present
        module_present = False
        for i in os.listdir():
            if i == "interceptor.ko":
                module_present = True
        if not module_present:
            print("kernel module not present")
            return -1

        try:
            self.pipe_read, self.pipe_write = os.pipe()
            pid = os.fork()
            if pid:
                os.close(self.pipe_read)
                self.target_pid = pid
                print('target_pid='+str(self.target_pid)+' inj_errno='+str(inj_errno)+' syscall_no='+str(syscall_no)+' invoc_count='+str(invoc_count))
                if self._check_load_lkm():
                    subprocess.call(['/bin/insmod',
                                     './interceptor.ko',
                                     'target_pid='+str(self.target_pid),
                                     'inj_errno='+str(inj_errno),
                                     'syscall_no='+str(syscall_no),
                                     'invoc_count='+str(invoc_count)])
                    if isdir('/sys/module/interceptor/parameters'):
                        self.sysFsObj = SysFsObject("/sys/module/interceptor/parameters")
                    else:
                        print('Sysfs directory not present')
                else:
                    if isdir('/sys/module/interceptor/parameters'):
                        self.sysFsObj = SysFsObject("/sys/module/interceptor/parameters")
                    else:
                        print('Sysfs directory not present')
                    self.sysFsObj.__setattr__("finished", 0)
                    self.sysFsObj.__setattr__("target_pid", self.target_pid)
                    self.sysFsObj.__setattr__("inj_errno", self.inj_errno)
                    self.sysFsObj.__setattr__("syscall_no", self.syscall_no)
                    self.sysFsObj.__setattr__("invoc_count", self.invoc_count)

                self.myDetector = FIDetector.FIDetector(self.target_pid)
                os.write(self.pipe_write, bytes(1))
                os.close(self.pipe_write)
            else:
                os.close(self.pipe_write)
                while not os.read(self.pipe_read, 1):
                    time.sleep(2)
                os.close(self.pipe_read)
                os.execv(self.exec_path, ["scfi_target"])
                sys.exit(0)
        except RuntimeError:
            print("Can't write Kernel parameter")
        except FIDetector.NoExecutableError:
            print("Executable not specified in FIDetector_config.conf!")
            return -1
        except LookupError as err:
            print("test_dir not a valid directory!")
            return -1
        except Exception as err:
            print('Execution path is not valid!')
            print(err.args)
            return -1

        ret = self.myDetector.check_once()
        p = psutil.Process(self.target_pid)
        p.kill()
        c_pid, status = os.waitpid(pid, 0)
        if os.WIFEXITED(status):
            print(os.WEXITSTATUS(status))
        self.status = os.WEXITSTATUS(status)
        print(str(c_pid)+" Status: "+str(os.WEXITSTATUS(status)))
        self.__log_fail(ret)
        print("")

    def __log_fail(self, ret):
        if ret == -1:
            with open(self.log_file, "a") as log_fd:
                log_fd.write("Crash: syscall_no = "+str(self.syscall_no)+" errno = "+str(self.inj_errno)+" invoc_count = "+str(self.invoc_count)+" status = "+str(self.status)+ "\n")
        if ret == -2:
            with open(self.log_file, "a") as log_fd:
                log_fd.write("Hang: syscall_no = "+str(self.syscall_no)+" errno = "+str(self.inj_errno)+" invoc_count = "+str(self.invoc_count)+" status = "+str(self.status)+ "\n")
        if ret == -3:
            with open(self.log_file, "a") as log_fd:
                log_fd.write("Failur: syscall_no = "+str(self.syscall_no)+" errno = "+str(self.inj_errno)+" invoc_count = "+str(self.invoc_count)+" status = "+str(self.status)+ "\n")
            
    def __read_config(self):
        with open('FIController_config.conf', 'r') as config_file:
            config = configparser.ConfigParser()
            config.readfp(config_file)
            try:
                self.exec_path = config.get('FIController', 'executable')
            except configparser.NoOptionError:
                print("No executable specified in FIController_config.conf")
                sys.exit(2)
            try:
                self.injection_map_loc = config.get('FIController', 'injection_map_loc')
            except configparser.NoOptionError:
                print("No injection_map_loc specified in FIController_config.conf")
                sys.exit(2)
            try:
                self.round_count = int(config.get('FIController', 'round_count'))
            except configparser.NoOptionError:
                print("No round_count specified in FIController_config.conf")
                sys.exit(2)
            try:
                self.log_file = str(config.get('FIController', 'log_file'))
            except configparser.NoOptionError:
                print("No log_file specified in FIController_config.conf")
                sys.exit(2)
            try:
                self.invoc_quant_map_loc = str(config.get('FIController', 'invoc_quant'))
            except configparser.NoOptionError:
                print("No invoc_quant specified in FIController_config.conf")
                sys.exit(2)
        self.__read_injection_map()
        self.__read_invoc_quant_map()
        return

    def __read_invoc_quant_map(self):
        with open(self.invoc_quant_map_loc, "r") as fd_map:
            reader = csv.reader(fd_map)
            self.invoc_quant_map = []
            for row in reader:
                self.invoc_quant_map.append(row)
        return

    def __read_injection_map(self):
        with open(self.injection_map_loc, "r") as fd_map:
            reader = csv.reader(fd_map)
            row_c = 0
            self.injection_map = []
            for row in reader:
                row_c = row_c + 1
                self.injection_map.append(row)
            self.possible_inj = row_c
        return

    def __random_injection(self):
        return random.randint(0, self.possible_inj-1)

    def __random_invoc_count(self, syscall_no):
        max_count = 20
        for elem in self.invoc_quant_map:
            if int(elem[0]) == int(syscall_no):
                max_count = int(elem[1])
        return random.randint(1, max_count)

    def __check_prev_use(self, invoc_vec):
        if self.used_param.get(invoc_vec[0]):
            for element in self.used_param[invoc_vec[0]]:
                if element[0] == invoc_vec[1] and element[1] == invoc_vec[2]:
                    return 0
        return 1

    def run_all(self):
        while len(self.injection_map) > 0:
            current_call_and_errno = self.injection_map.pop(0)
            max_count=20
            for elem in self.invoc_quant_map:
                if int(elem[0]) == int(current_call_and_errno[0]):
                    max_count = int(elem[1])
            for current_invoc_count in range(1, max_count+1):
                self.inject_once(int(current_call_and_errno[0]),
                                 int(current_call_and_errno[1]), 
                                 int(current_invoc_count))
    def inject_run(self):
        for x in range(0, self.round_count):
            invoc_vec = []
            while True:
                chose = self.__random_injection()
                invoc_vec = self.injection_map[chose]
                invoc_vec.append(self.__random_invoc_count(int(invoc_vec[0])))
                if self.__check_prev_use(invoc_vec):
                    if self.used_param.get(invoc_vec[0]):
                        self.used_param[invoc_vec[0]].append([invoc_vec[1], invoc_vec[2]])
                    else:
                        self.used_param[invoc_vec[0]] = []
                        self.used_param[invoc_vec[0]].append([invoc_vec[1], invoc_vec[2]])
                    break
            self.inject_once(int(invoc_vec[0]), int(invoc_vec[1]), int(invoc_vec[2]))


    # CHANGE THIS IF MORE THAN ONE FAILURE SHOULD BE INJECTED
    #def __del__(self):
        #subprocess.call(['/bin/rmmod', 'interceptor'])
        #print('Module removed successfully.')

def main():
    myController = FIController()
    myController.run_all()
    #myController.inject_run()
#    try:
#        opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["exec_path=","syscall_no=","inj_errno=","invoc_count="])
#    except getopt.GetoptError as err:
#        print("contoller.py --exec_path <path_to_program> --syscall_no <number> --inj_errno <errno> --invoc_count <count>")
#        sys.exit(2)
#
#    exec_path   = ''
#    syscall_no  = 0
#    inj_errno   = 0
#    invoc_count = 0
#    for opt, arg in opts:
#        if opt == "--help":
#            print("contoller.py --exec_path <path_to_program> --syscall_no <number> --inj_errno <errno> --invoc_count <count>")
#            sys.exit(2)
#        elif opt == "--exec_path":
#            exec_path = arg
#        elif opt == "--syscall_no":
#            syscall_no = arg
#        elif opt == "--inj_errno":
#            inj_errno = arg
#        elif opt == "--invoc_count":
#            invoc_count = arg
#
#    if not exec_path or not syscall_no or not inj_errno or not invoc_count:
#        print("contoller.py --exec_path <path_to_program> --syscall_no <number> --inj_errno <errno> --invoc_count <count>")
#        sys.exit(2)
#
#    myController = FIController(exec_path)
#    myController.inject_once(syscall_no, inj_errno, invoc_count)

if __name__ == "__main__":
    main()
